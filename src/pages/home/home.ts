import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, AlertController, ToastController, Slides } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import * as Constant from '../../config/constants';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';

import { ProfilePage } from '../profile/profile';
import { PostProductPage } from '../post-product/post-product';
import { PostDetailPage } from '../post-detail/post-detail';
import { LoginPage } from '../login/login';
import { SettingPage } from '../setting/setting';


@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})


export class HomePage {
	@ViewChild(Slides) slides: Slides;

	lists: Array<any>;
	first: number;
	base_url: any;
	user_id: any = null;
	county: any = null;
	cities: any = null;
	slider_lists: Array<any>;
	user:any='';

	constructor(
		private socialSharing: SocialSharing,
		private callNumber: CallNumber,
		public events: Events,
		public navCtrl: NavController,
		public toastCtrl: ToastController,
		public http: Http, public storage: Storage,
		public alertCtrl: AlertController,
		public modalCtrl: ModalController) {

		this.base_url = Constant.domainConfig.base_url;

		this.lists = new Array();
		this.slider_lists = new Array();

		this.events.subscribe('local: change', () => {
			this.ionViewWillEnter();
		})
		this.events.subscribe('user: change', () => {
			this.ionViewWillEnter();
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad LoginPage');

	}

	ionViewDidEnter() {

	}


	ionViewWillEnter() {
		console.log('ionViewWillEnter TabHomePage');

		this.first = -5;
		this.lists = new Array();

		this.county = null;
		this.cities = null;

		this.storage.ready().then(() => {
			this.storage.get('user').then((obj) => {
				console.log(obj);
				if (obj != null) {
					this.user_id = obj.id;
				} else {
					this.user_id = null;
				}
				this.http.get(this.base_url + 'api/products_api/slider?user_lg=' + this.user_id).subscribe(data => {
					console.log(data.json());
					this.slider_lists = data.json();
					if (this.slider_lists.length > 1) {

					}
				}, error => {

				})
			});
			this.storage.get('local').then((data) => {
				console.log(data);
				if (data != null && data.all_local == false) {
					this.county = data.county;
					this.cities = data.cities;
				}
				this.loadMore();
			});
		});
	}

	loadMore(infiniteScroll: any = null) {
		this.first += 5;
		this.http.get(this.base_url + 'api/products_api/products?private=0&order=last&first=' + this.first + '&offset=5' + '&county_id=' + this.county + '&cities_id=' + this.cities + '&user_lg=' + this.user_id).subscribe(data => {
			console.log(data.json());
			let jsonData = data.json();
			for (var i = 0; i < jsonData.length; i++) {
				this.lists.push(jsonData[i]);
			}
			if (infiniteScroll) {
				infiniteScroll.complete();
			}
		}, error => {
			if (infiniteScroll != null) {
				infiniteScroll.enable(false);
			}
		})
	}

	setting_slides() {
		this.slides.autoplayDisableOnInteraction = false;
	}

	setting() {
		let modal = this.modalCtrl.create(SettingPage);
		modal.present();
	}

	up_product() {
		if (this.user_id != null && this.user_id != 0) {
			let modal = this.modalCtrl.create(PostProductPage);
			modal.present();
		} else {
			let modal = this.modalCtrl.create(LoginPage);
			modal.present();
		}
	}

	report(product_id, i) {
		if (this.user_id != null && this.user_id != 0) {
			let headers: Headers = new Headers({
				'Content-Type': 'application/x-www-form-urlencoded'
			});
			this.http.post(this.base_url + 'api/products_api/report', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(data => {
				console.log(data.json());
				this.lists[i].report = data.json().report;
			}, error => {

			})
		} else {
			let modal = this.modalCtrl.create(LoginPage);
			modal.present();
		}
	}

	call(phone: string) {
		this.callNumber.callNumber(phone, true)
			.then(() => console.log('Launched dialer!'))
			.catch(() => console.log('Error launching dialer'));
	}

	share(item) {
		this.socialSharing.share(item.name, item.content, null, Constant.domainConfig.base_url + 'detail?id=' + item.id);
	}

	favorites(product_id, i) {
		if (this.user_id != null && this.user_id != 0) {
			let headers: Headers = new Headers({
				'Content-Type': 'application/x-www-form-urlencoded'
			});
			this.http.post(this.base_url + 'api/favoriest_api/add_un_favorites', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(data => {
				console.log(data.json());
				this.lists[i].favorites = data.json().favorites;
			}, error => {

			})
		} else {
			let modal = this.modalCtrl.create(LoginPage);
			modal.present();
		}
	}

	mail(item) {
		console.log(item);
		if (this.user_id == null) {
			//go to login page
			let modal = this.modalCtrl.create(LoginPage);
			modal.present();
			return;
		}
		let promtDialog = this.alertCtrl.create({
			title: 'Messages',
			inputs: [
				{
					name: 'message',
					placeholder: 'Message'
				}
			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Send',
					handler: data => {
						///send message via email system
						this.storage.ready().then(() => {
							this.storage.get('user').then((obj) => {
								if(obj.email==item.email){
									let alert=this.alertCtrl.create({
										message:"you cannot send mail to yourself",
										buttons:['Dismiss']
									})
									alert.present();
									return;
								}
								let post_data = 'email=' + item.email + '&message=' + data.message + '&user_name=' + obj.user_name + '&reply_to=' + obj.email;
								let headers: Headers = new Headers({
									'Content-Type': 'application/x-www-form-urlencoded'
								});
								this.http.post(this.base_url + 'api/users_api/send_enquiry', post_data, { headers: headers }).subscribe(data => {
									if (data.json().success == 0) {
										let toast = this.toastCtrl.create({
											message: 'you just can send enquiry after 120 seconds',
											duration: 3000
										});
										toast.present();
									} else {
										let toast = this.toastCtrl.create({
											message: 'Your messages have been sent, thank you !!!',
											duration: 3000
										});
										toast.present();
									}
								}, error => {

								})
							})
						})
					}
				}
			]
		})
		promtDialog.present();
	}

	view(item) {
		this.navCtrl.push(PostDetailPage, { item: item });
	}

	view_prf(id) {
		this.navCtrl.push(ProfilePage, { user_id: id });
	}

}
