import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { HomePage } from '../home/home';
import { FavoritesPage } from '../favorites/favorites';
import { CategoriesPage } from '../categories/categories';
import { SearchPage } from '../search/search';
import { ProfilePage } from '../profile/profile';
import { LoginPage } from '../login/login';

import { Events } from 'ionic-angular';

@Component({
  selector: 'tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = SearchPage;
  tab3Root = CategoriesPage;
  tab4Root = FavoritesPage;
  tab5Root = ProfilePage;
  tab6Root = LoginPage;

  constructor(public storage: Storage, public events:Events, public navCtrl: NavController) {

  }

  ionViewWillEnter(){
    console.log('ionViewWillEnter TabsPage');
  }

}
