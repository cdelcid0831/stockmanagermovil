import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpModule, Http } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { SocialSharing } from '@ionic-native/social-sharing';
import { CallNumber } from '@ionic-native/call-number';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { SearchPage } from '../pages/search/search';
import { FavoritesPage } from '../pages/favorites/favorites';
import { CategoriesPage } from '../pages/categories/categories';

import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { SettingPage } from '../pages/setting/setting';
import { PostProductPage } from '../pages/post-product/post-product';
import { PostDetailPage } from '../pages/post-detail/post-detail';
import { CategoriesDetailPage } from '../pages/categories-detail/categories-detail';
import { TabsPage } from '../pages/tabs/tabs';
import { ActivationCodePage } from '../pages/activation-code/activation-code';
import { EditProductPage } from '../pages/edit-product/edit-product';
import { ModalRatingPage } from '../pages/modal-rating/modal-rating';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

import { FileTransfer } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { OneSignal } from '@ionic-native/onesignal';


/*translate loader*/
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
/*end translate loader*/

export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
    declarations: [
        MyApp,
        AboutPage,
        HomePage,
        SearchPage,
        FavoritesPage,
        CategoriesPage,
        ProfilePage,
        LoginPage,
        SignupPage,
        SettingPage,
        PostProductPage,
        PostDetailPage,
        CategoriesDetailPage,
        ActivationCodePage,
        EditProductPage,
        TabsPage,
        ModalRatingPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        Ionic2RatingModule,
        IonicStorageModule.forRoot(),
        IonicModule.forRoot(MyApp),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [Http]
            }
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        AboutPage,
        HomePage,
        SearchPage,
        FavoritesPage,
        CategoriesPage,
        ProfilePage,
        LoginPage,
        SignupPage,
        SettingPage,
        PostProductPage,
        PostDetailPage,
        CategoriesDetailPage,
        ActivationCodePage,
        EditProductPage,
        TabsPage,
        ModalRatingPage
    ],
    providers: [
        StatusBar,
        Camera,
        SplashScreen,
        SocialSharing,
        CallNumber,
        HttpModule,
        FileTransfer,
        File,
        FilePath,
        Facebook,
        OneSignal,
        { provide: ErrorHandler, useClass: IonicErrorHandler }
    ]
})
export class AppModule { }
