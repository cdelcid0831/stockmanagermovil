webpackJsonp([0],{

/***/ 131:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 131;

/***/ }),

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return domainConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return onesignal_app_id; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return google_project_number; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return fb_app; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return fb_v; });
var domainConfig = {
    base_url: 'http://lrandomdev.com/demo/chili/'
};
var onesignal_app_id = 'd9b023b3-815e-4596-9d35-272b478531aa';
var google_project_number = '762391382612';
var fb_app = 1552011421763113;
var fb_v = "2.5";
//# sourceMappingURL=constants.js.map

/***/ }),

/***/ 173:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 173;

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__ = __webpack_require__(225);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(modalCtrl, viewCtrl, navCtrl, navParams, events, http, storage, fb, alertCtrl) {
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.http = http;
        this.storage = storage;
        this.fb = fb;
        this.alertCtrl = alertCtrl;
        this.base_url = '';
        this.user_name = '';
        this.password = '';
        this.msg_err = null;
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.fb.browserInit(__WEBPACK_IMPORTED_MODULE_3__config_constants__["b" /* fb_app */], __WEBPACK_IMPORTED_MODULE_3__config_constants__["c" /* fb_v */]);
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        console.log('ionViewWillEnter LoginPage');
        this.msg_err = null;
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        if (this.user_name != null && this.user_name != '' && this.password != null && this.password != '') {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/users_api/login', 'user_name=' + this.user_name + '&pwd=' + this.password, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                if (data.json().empty != 1) {
                    var user = data.json()[0];
                    _this.storage.set('user', user);
                    _this.events.publish('user: change');
                    _this.navCtrl.pop();
                }
                else {
                    _this.msg_err = 'Your username or password is wrong';
                }
            }, function (error) {
            });
        }
        else {
            this.msg_err = 'You enter not enough information';
        }
    };
    LoginPage.prototype.fb_login = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            var params = new Array();
            _this.fb.api('/me?fields=name,gender,email', params).then(function (user) {
                console.log(JSON.stringify(user));
                _this.http.get(_this.base_url + 'api/users_api/facebook_user_check?fb_id=' + user.id).subscribe(function (data) {
                    //check user facebook
                    if (data.json().success == 0) {
                        //if do not have user, insert it 
                        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        });
                        var username = user.email.substring(0, user.email.indexOf("@"));
                        //console.log(username.substring(0,4));
                        username = username.substring(0, 4) + '_' + user.id;
                        var params_1 = 'fb_id=' + user.id + '&email=' + user.email + '&fullname=' + user.name + '&user_name=' + username;
                        _this.http.post(_this.base_url + 'api/users_api/facebook_user_register', params_1, { headers: headers }).subscribe(function (data) {
                            console.log(JSON.stringify(data));
                            var user = data.json();
                            user = user[0];
                            _this.storage.set('user', user);
                            _this.events.publish('user: change');
                            _this.navCtrl.pop();
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    else {
                        var user_1 = data.json().data[0];
                        _this.storage.set('user', user_1);
                        _this.events.publish('user: change');
                        _this.navCtrl.pop();
                        //console.log(JSON.stringify(data.json().data[0]));
                    }
                }, function (error) {
                });
            }).catch(function (e) {
                alert("Facebook login failed, try again !!!" + JSON.stringify(e));
            });
        })
            .catch(function (e) {
            alert("Facebook login failed, try again !!!" + JSON.stringify(e));
        });
    };
    LoginPage.prototype.signup = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
        modal.present();
    };
    // forgot() {
    //   let prompt = this.alertCtrl.create({
    //     title: 'Get new password via ?',
    //     inputs: [
    //       {
    //         type: 'radio',
    //         label: 'SMS',
    //         value: '0'
    //       },
    //       {
    //         type: 'radio',
    //         label: 'Mail',
    //         value: '1'
    //       }
    //     ],
    //     buttons: [
    //       {
    //         text: "Cancel",
    //         handler: data => {
    //         }
    //       },
    //       {
    //         text: "OK",
    //         handler: data => {
    //           if (data == 0) {
    //             //send sms
    //             this.showInputPhone(data);
    //           } else {
    //             //send mail
    //             this.showInputMail(data);
    //           }
    //         }
    //       }
    //     ]
    //   });
    //   prompt.present();
    // }
    // showInputPhone(type) {
    //   let prompt = this.alertCtrl.create({
    //     title: 'Input your phone ',
    //     inputs: [
    //       {
    //         name: 'phone',
    //         placeholder: 'Your Phone'
    //       }
    //     ],
    //     buttons: [
    //       {
    //         text: 'Cancel',
    //         role: 'cancel',
    //         handler: () => {
    //           console.log('Cancel clicked');
    //         }
    //       },
    //       {
    //         text: 'OK',
    //         handler: (data) => {
    //           console.log(data);
    //           this.reset_pwd(type,data.phone);
    //         }
    //       }
    //     ]
    //   })
    //   prompt.present()
    // }
    // showInputMail(type) {
    //   let prompt = this.alertCtrl.create({
    //     title: 'Input your mail',
    //     inputs: [
    //       {
    //         name: 'mail',
    //         placeholder: 'Your Mail'
    //       }
    //     ],
    //     buttons: [
    //       {
    //         text: 'Cancel',
    //         role: 'cancel',
    //         handler: () => {
    //           console.log('Cancel clicked');
    //         }
    //       },
    //       {
    //         text: 'OK',
    //         handler: (data) => {
    //           console.log(data);
    //           this.reset_pwd(type,data.mail);
    //         }
    //       }
    //     ]
    //   })
    //   prompt.present()
    // }
    // reset_pwd(type,data) {
    //   let headers: Headers = new Headers({
    //     'Content-Type': 'application/x-www-form-urlencoded'
    //   });
    //   let params='type='+type+'&data='+data;
    //   this.http.post(this.base_url + 'api/users_api/reset_pwd', params, { headers: headers }).subscribe(data => {
    //   }, error => {
    //   })
    // }
    LoginPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/login/login.html"*/'<ion-content>\n  <div class="main-login">\n    <button ion-button icon-only clear class="btn-close" round (click)="dismiss()">\n      <ion-icon name="ios-close-circle-outline"></ion-icon>\n    </button>\n    <div class="wrapper">\n      <img class="logo" src="assets/img/logo_chili_w.png">\n      <img class="bg-img" src="assets/img/decor_login.png" alt="">\n      \n      <div class="main-form">\n      	<ion-list>\n      		<ion-item>\n        		<ion-input placeholder="{{\'username\' | translate}}" [(ngModel)]="user_name" type="text"></ion-input>\n      		</ion-item>\n\n      		<ion-item>\n        		<ion-input placeholder="{{\'password\' | translate}}" [(ngModel)]="password" type="password"></ion-input>\n      		</ion-item>\n\n          <b *ngIf="msg_err != null" class="msg_err">{{msg_err}}</b>\n    		  \n          <ion-item>\n      			<button ion-button block round color="danger" (click)="login()">{{\'login\' | translate}}</button>\n      		</ion-item>\n\n          <ion-item>\n            <button ion-button block round color="info" (click)="fb_login()">{{\'login_with_facebook\' | translate}}</button>\n          </ion-item>\n\n\n          <ion-item>\n            <button ion-button block round color="dark" (click)="signup()">{{\'signup\' | translate}}</button>\n          </ion-item>\n<!-- \n          <p (click)="forgot()">Forgot your password?</p> -->\n      	</ion-list>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__["a" /* Facebook */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__favorites_favorites__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__categories_categories__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__search_search__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__profile_profile__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var TabsPage = (function () {
    function TabsPage(storage, events, navCtrl) {
        this.storage = storage;
        this.events = events;
        this.navCtrl = navCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_6__search_search__["a" /* SearchPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_5__categories_categories__["a" /* CategoriesPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__favorites_favorites__["a" /* FavoritesPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_7__profile_profile__["a" /* ProfilePage */];
        this.tab6Root = __WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */];
    }
    TabsPage.prototype.ionViewWillEnter = function () {
        console.log('ionViewWillEnter TabsPage');
    };
    return TabsPage;
}());
TabsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'tabs',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="" tabIcon="ios-home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="" tabIcon="ios-search"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="" tabIcon="md-list-box"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="" tabIcon="ios-heart"></ion-tab>\n  <ion-tab [root]="tab5Root" tabTitle="" tabIcon="ios-contact"></ion-tab>\n  <ion-tab [root]="tab6Root" tabTitle="" tabIcon=""></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/tabs/tabs.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]])
], TabsPage);

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__profile_profile__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__post_product_post_product__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__post_detail_post_detail__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__setting_setting__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var HomePage = (function () {
    function HomePage(socialSharing, callNumber, events, navCtrl, toastCtrl, http, storage, alertCtrl, modalCtrl) {
        var _this = this;
        this.socialSharing = socialSharing;
        this.callNumber = callNumber;
        this.events = events;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.user_id = null;
        this.county = null;
        this.cities = null;
        this.user = '';
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.lists = new Array();
        this.slider_lists = new Array();
        this.events.subscribe('local: change', function () {
            _this.ionViewWillEnter();
        });
        this.events.subscribe('user: change', function () {
            _this.ionViewWillEnter();
        });
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    HomePage.prototype.ionViewDidEnter = function () {
    };
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewWillEnter TabHomePage');
        this.first = -5;
        this.lists = new Array();
        this.county = null;
        this.cities = null;
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                console.log(obj);
                if (obj != null) {
                    _this.user_id = obj.id;
                }
                else {
                    _this.user_id = null;
                }
                _this.http.get(_this.base_url + 'api/products_api/slider?user_lg=' + _this.user_id).subscribe(function (data) {
                    console.log(data.json());
                    _this.slider_lists = data.json();
                    if (_this.slider_lists.length > 1) {
                    }
                }, function (error) {
                });
            });
            _this.storage.get('local').then(function (data) {
                console.log(data);
                if (data != null && data.all_local == false) {
                    _this.county = data.county;
                    _this.cities = data.cities;
                }
                _this.loadMore();
            });
        });
    };
    HomePage.prototype.loadMore = function (infiniteScroll) {
        var _this = this;
        if (infiniteScroll === void 0) { infiniteScroll = null; }
        this.first += 5;
        this.http.get(this.base_url + 'api/products_api/products?private=0&order=last&first=' + this.first + '&offset=5' + '&county_id=' + this.county + '&cities_id=' + this.cities + '&user_lg=' + this.user_id).subscribe(function (data) {
            console.log(data.json());
            var jsonData = data.json();
            for (var i = 0; i < jsonData.length; i++) {
                _this.lists.push(jsonData[i]);
            }
            if (infiniteScroll) {
                infiniteScroll.complete();
            }
        }, function (error) {
            if (infiniteScroll != null) {
                infiniteScroll.enable(false);
            }
        });
    };
    HomePage.prototype.setting_slides = function () {
        this.slides.autoplayDisableOnInteraction = false;
    };
    HomePage.prototype.setting = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_11__setting_setting__["a" /* SettingPage */]);
        modal.present();
    };
    HomePage.prototype.up_product = function () {
        if (this.user_id != null && this.user_id != 0) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__post_product_post_product__["a" /* PostProductPage */]);
            modal.present();
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    HomePage.prototype.report = function (product_id, i) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/products_api/report', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.lists[i].report = data.json().report;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    HomePage.prototype.call = function (phone) {
        this.callNumber.callNumber(phone, true)
            .then(function () { return console.log('Launched dialer!'); })
            .catch(function () { return console.log('Error launching dialer'); });
    };
    HomePage.prototype.share = function (item) {
        this.socialSharing.share(item.name, item.content, null, __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'detail?id=' + item.id);
    };
    HomePage.prototype.favorites = function (product_id, i) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/favoriest_api/add_un_favorites', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.lists[i].favorites = data.json().favorites;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    HomePage.prototype.mail = function (item) {
        var _this = this;
        console.log(item);
        if (this.user_id == null) {
            //go to login page
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__login_login__["a" /* LoginPage */]);
            modal.present();
            return;
        }
        var promtDialog = this.alertCtrl.create({
            title: 'Messages',
            inputs: [
                {
                    name: 'message',
                    placeholder: 'Message'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        ///send message via email system
                        _this.storage.ready().then(function () {
                            _this.storage.get('user').then(function (obj) {
                                if (obj.email == item.email) {
                                    var alert_1 = _this.alertCtrl.create({
                                        message: "you cannot send mail to yourself",
                                        buttons: ['Dismiss']
                                    });
                                    alert_1.present();
                                    return;
                                }
                                var post_data = 'email=' + item.email + '&message=' + data.message + '&user_name=' + obj.user_name + '&reply_to=' + obj.email;
                                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                });
                                _this.http.post(_this.base_url + 'api/users_api/send_enquiry', post_data, { headers: headers }).subscribe(function (data) {
                                    if (data.json().success == 0) {
                                        var toast = _this.toastCtrl.create({
                                            message: 'you just can send enquiry after 120 seconds',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                    else {
                                        var toast = _this.toastCtrl.create({
                                            message: 'Your messages have been sent, thank you !!!',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                }, function (error) {
                                });
                            });
                        });
                    }
                }
            ]
        });
        promtDialog.present();
    };
    HomePage.prototype.view = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__post_detail_post_detail__["a" /* PostDetailPage */], { item: item });
    };
    HomePage.prototype.view_prf = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__profile_profile__["a" /* ProfilePage */], { user_id: id });
    };
    return HomePage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_12" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Slides */])
], HomePage.prototype, "slides", void 0);
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button clear small color="light" ion-button class="setting-nav" (click)="setting()">\n      <ion-icon name="ios-pin-outline"></ion-icon>\n    </button>\n    <div class="brand">\n      <!-- <ion-title>Home</ion-title> -->\n      <img src="assets/img/logo_chili.png" alt="">\n    </div>\n    <button clear small color="light" ion-button class="upload-nav" (click)="up_product()">\n      <ion-icon name="ios-paper-plane-outline"></ion-icon>\n    </button>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="slider_lists.length > 1">\n    <ion-slides pager="true" effect="coverflow" initialSlide="0" (ionSlideAutoplayStart)=\'setting_slides()\' autoplay="500" loop="true" speed="2000">\n      <ion-slide *ngFor="let item of slider_lists ; let i = index">\n        <img src="{{base_url + item.image_path}}" alt="">\n        <div class="slogan">\n          <div class="wrapper-slogan" padding>\n            <h2>{{item.title.slice(0,25) + \'...\'}}</h2>\n            <p>{{item.content.slice(0,65) + \'...\'}}</p>\n            <button ion-button round icon-left small color="light" (click)="view(item)">\n              <ion-icon name="md-search"></ion-icon>{{\'detail\' | translate }}\n            </button>\n            <button ion-button round icon-left small color="light" (click)="call(item.phone)">\n              <ion-icon name="ios-call"></ion-icon>{{\'now\' | translate }}\n            </button>\n          </div>\n        </div>\n      </ion-slide>\n    </ion-slides>\n  </div>\n  <div *ngIf="lists.empty == 1" class="empty_data">\n    <div>\n      <h4>not found</h4>\n      <p>There are currently no products available in the area you require!</p>\n    </div>\n  </div>\n  <ion-list class="lst-products" *ngIf="lists.empty != 1">\n    <div class="decor">\n      <img src="assets/img/decor.png" alt="">\n    </div>\n    <ion-card *ngFor="let item of lists ; let i = index">\n      <ion-item>\n        <ion-avatar item-start (click)="view_prf(item.user_id)">\n        <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')==-1" src="{{base_url + item.avt}}">\n        <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')!=-1" src="{{item.avt}}">\n        <img *ngIf="item.avt == null" src="assets/avt.png">\n\n        </ion-avatar>\n        <h2><b (click)="view_prf(item.user_id)">{{item.user_name}}</b> posted {{item.created_at}}</h2>\n        <div class="more">\n          <button [ngClass]="{\'active\' : (item.report == true)}" ion-button icon-left clear (click)="report(item.id, i)">\n            <ion-icon name="ios-flag-outline"></ion-icon>\n          </button>\n        </div>\n      </ion-item>\n      <img src="{{base_url + item.image_path}}" (click)="view(item)" />\n      <ion-card-content (click)="view(item)">\n        <ion-card-title>\n          {{item.title}}\n        </ion-card-title>\n        <rating [(ngModel)]="item.rating" readOnly="true" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star" nullable="false">\n        </rating>\n        <div class="local">\n          <ion-icon name="md-locate"></ion-icon>\n          {{item.county_name + \' - \' + item.cities_name}}\n        </div>\n        <p class="price">price: {{item.price}}</p>\n        <p>{{item.content.slice(0,107) + \'...\'}}</p>\n      </ion-card-content>\n      <ion-row>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="mail(item)">\n            <ion-icon name="ios-mail"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="call(item.phone)">\n            <ion-icon name="ios-call"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button [ngClass]="{\'active\' : (item.favorites == true)}" ion-button icon-left clear (click)="favorites(item.id, i)">\n            <ion-icon name="ios-heart"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="share(item)">\n            <ion-icon name="md-share"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)="loadMore($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__validators_email__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__validators_phone__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__validators_username__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__validators_password__ = __webpack_require__(316);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SignupPage = (function () {
    function SignupPage(navCtrl, viewCtrl, http, alertCtrl, toastCtrl, formBuilder, navParams) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.navParams = navParams;
        this.post = {
            user_name: '',
            full_name: '',
            email: '',
            address: '',
            pwd: '',
            repwd: '',
            phone: '',
            send_code_method: 0
        };
        this.base_url = '';
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.form = formBuilder.group({
            full_name: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(50), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].pattern('[a-zA-Z ]*'), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            user_name: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_7__validators_username__["a" /* UserNameValidator */].isValid, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(50)])],
            email: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_5__validators_email__["a" /* EmailValidator */].isValid, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            phone: ['', __WEBPACK_IMPORTED_MODULE_6__validators_phone__["a" /* PhoneValidator */].isValid],
            address: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(200), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required])],
            pwd: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].maxLength(50)])],
            repwd: ['', __WEBPACK_IMPORTED_MODULE_8__validators_password__["a" /* PasswordValidator */].isMatch],
            send_code_method: ['0']
        });
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SignupPage.prototype.signup = function () {
        var _this = this;
        if (this.post.user_name == '') {
        }
        var signup_url = '';
        if (this.post.send_code_method == 0) {
            //if SMS method
            signup_url = this.base_url + 'api/users_api/check_sms_register_valid';
        }
        else {
            //if mail method
            signup_url = this.base_url + 'api/users_api/check_mail_register_valid';
        }
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var data = 'user_name=' + this.form.value.user_name +
            '&full_name=' + this.form.value.full_name +
            '&email=' + this.form.value.email +
            '&address=' + this.form.value.address +
            '&pwd=' + this.form.value.pwd + '&phone=' + this.form.value.phone;
        this.http.post(signup_url, data, { headers: headers }).subscribe(function (data) {
            console.log(data);
            var jsonData = data.json();
            if (jsonData.success == 3) {
                //if success go to verification page
                var alertCtrl_1 = _this.alertCtrl.create({
                    title: 'Please enter your verification code',
                    enableBackdropDismiss: false,
                    inputs: [
                        {
                            name: 'code',
                            placeholder: 'verification code'
                        }
                    ],
                    buttons: [
                        {
                            text: 'Cancel',
                            handler: function (data) {
                                var post_data = 'email=' + _this.form.value.email;
                                _this.http.post(_this.base_url + 'api/users_api/cancel_register', post_data, { headers: headers }).subscribe(function (data) {
                                    _this.navCtrl.pop();
                                });
                            }
                        },
                        {
                            text: 'Resend',
                            handler: function (data) {
                                var post_data = 'email=' + _this.form.value.email + '&phone=' + _this.form.value.phone + '&send_code_method=' + _this.form.value.send_code_method;
                                _this.http.post(_this.base_url + 'api/users_api/resend_verified_code', post_data, { headers: headers }).subscribe(function (data) {
                                });
                            }
                        },
                        {
                            text: 'Submit',
                            handler: function (data) {
                                var post_data = 'code=' + data.code + '&email=' + _this.form.value.email;
                                _this.http.post(_this.base_url + 'api/users_api/register', post_data, { headers: headers }).subscribe(function (data) {
                                    if (data.json().success == 1) {
                                        //register done
                                        var confirmCtl = _this.alertCtrl.create({
                                            message: 'Signup Successfully, Login now !!!',
                                            buttons: [{
                                                    text: 'Ok',
                                                    handler: function () {
                                                        alertCtrl_1.dismiss();
                                                        _this.navCtrl.pop();
                                                    }
                                                }]
                                        });
                                        confirmCtl.present();
                                    }
                                    else {
                                        //register failed
                                        var toastCtrl = _this.toastCtrl.create({
                                            message: 'your confirm code wrong, pls try again',
                                            duration: 3000,
                                            position: 'top'
                                        });
                                        toastCtrl.present();
                                    } //end if else
                                });
                                return false;
                            }
                        }
                    ]
                });
                alertCtrl_1.present();
            }
            if (jsonData.success == 1) {
                var alertCtrl = _this.alertCtrl.create({
                    message: 'UserName exist, try another !!',
                    buttons: ['Dismiss']
                });
                alertCtrl.present();
            }
            if (jsonData.success == 0) {
                var alertCtrl = _this.alertCtrl.create({
                    message: 'Email exist, try another !!',
                    buttons: ['Dismiss']
                });
                alertCtrl.present();
            }
            if (jsonData.success == 2) {
                var alertCtrl = _this.alertCtrl.create({
                    message: 'Phone exist, try another !!',
                    buttons: ['Dismiss']
                });
                alertCtrl.present();
            }
        }, function (error) {
            console.log(error);
        });
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/signup/signup.html"*/'<ion-content padding>\n   <button ion-button icon-only clear class="btn-close" round (click)="dismiss()">\n      <ion-icon name="ios-close-circle-outline"></ion-icon>\n   </button>\n   <ion-list>\n     <form [formGroup]="form" (ngSubmit)="signup()">\n      <h4 style="text-align: center;"> {{\'signup\' | translate }}</h4>\n      <ion-item>\n         <ion-input placeholder="{{\'username\' | translate}}" formControlName="user_name" type="text"></ion-input>\n      </ion-item>\n      <p class="err"  *ngIf="!form.controls.user_name.valid  && (form.controls.user_name.dirty || submitAttempt)">* More than 5 and less than 50 characters</p>\n\n      <ion-item>\n         <ion-input placeholder="{{\'fullname\' | translate}}" formControlName="full_name"  type="text"></ion-input>\n      </ion-item>\n      <p class="err"  *ngIf="!form.controls.full_name.valid  && (form.controls.full_name.dirty || submitAttempt)">* More than 5 and less than 50 characters</p>\n\n      <ion-item>\n         <ion-input placeholder="{{\'email\' | translate}}" formControlName="email"  type="text"></ion-input>\n      </ion-item>\n      <p class="err"  *ngIf="!form.controls.email.valid  && (form.controls.email.dirty || submitAttempt)">* More than 5 and less than 50 characters</p>\n\n   \n      <ion-item>\n         <ion-input placeholder="{{\'phone\' | translate}}" formControlName="phone"  type="text"></ion-input>\n      </ion-item>\n          <p class="err"  *ngIf="!form.controls.phone.valid  && (form.controls.phone.dirty || submitAttempt)">* More than 5 and less than 50 characters</p>\n    \n      <ion-item>\n         <ion-input placeholder="{{\'address\' | translate}}" formControlName="address" type="text"></ion-input>\n      </ion-item>\n        <p class="err"  *ngIf="!form.controls.address.valid && (form.controls.address.dirty || submitAttempt)">* More than 5 and less than 50 characters</p>\n   \n      <ion-item>\n         <ion-input placeholder="{{\'password\' | translate}}" formControlName="pwd" type="password"></ion-input>\n      </ion-item> \n         <p class="err"  *ngIf="!form.controls.pwd.valid && (form.controls.pwd.dirty || submitAttempt)">* More than 5 and less than 50 characters</p>\n\n      <ion-item>\n         <ion-input placeholder="{{\'repassword\' | translate}}" formControlName="repwd"  type="password"></ion-input>\n      </ion-item>\n       <p class="err"  *ngIf="!form.controls.repwd.valid  && (form.controls.repwd.dirty || submitAttempt)">* More than 5 and less than 50 characters</p>\n\n      <ion-list radio-group formControlName="send_code_method">\n         <ion-list-header>\n           {{\'get_verified_code_via\' | translate }}\n         </ion-list-header>\n         <ion-item>\n            <ion-label><span style="font-size: 12px">{{\'sms\' | translate}}</span></ion-label>\n            <ion-radio value="0"></ion-radio>\n         </ion-item>\n         <ion-item>\n            <ion-label><span style="font-size: 12px">{{\'mail\' | translate}}</span></ion-label>\n            <ion-radio value="1"></ion-radio>\n         </ion-item>\n    </ion-list>\n\n\n      <ion-item>\n         <button ion-button block round color="danger" [disabled]="!form.valid" type="submit" >{{\'submit\' | translate}}</button>\n      </ion-item>\n      </form>\n   </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/signup/signup.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], SignupPage);

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalRatingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ModalRatingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ModalRatingPage = (function () {
    function ModalRatingPage(navCtrl, http, navParams, alertCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.point = 5;
        this.product_id = this.navParams.get('product_id');
        this.product_user_id = this.navParams.get('product_user_id');
        this.user_id = this.navParams.get('user_id');
    }
    ModalRatingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalRatingPage');
    };
    ModalRatingPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalRatingPage.prototype.rate = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var post_data = 'product_id=' + this.product_id + '&user_id=' + this.user_id + '&product_user_id=' + this.product_user_id + '&point=' + this.point + '&message=' + this.message;
        this.http.post(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'api/products_api/rate', post_data, { headers: headers }).subscribe(function (data) {
        });
    };
    return ModalRatingPage;
}());
ModalRatingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-modal-rating',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/modal-rating/modal-rating.html"*/'<ion-content class="no-scroll">\n  <ion-row class="close-modal" (click)="dismiss()"></ion-row>\n  <ion-row class="main-modal" col-2>\n    <div class="wrapper">\n      <h6>{{\'write_your_review\' | translate}}</h6>\n      <rating [(ngModel)]="point" readOnly="false" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star" nullable="false">\n      </rating>\n      <div>\n        <ion-item>\n          <textarea [(ngModel)]="message" placeholder="{{\'content\' | translate}}" style="resize:none"></textarea>\n        </ion-item>\n        <button class="btn-rate" ion-button full round color="dark" (click)="rate()">{{\'rate\' | translate}}</button>\n      </div>\n    </div>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/modal-rating/modal-rating.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
], ModalRatingPage);

//# sourceMappingURL=modal-rating.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditProductPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the EditProductPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EditProductPage = (function () {
    function EditProductPage(navCtrl, navParams, http, actionSheetCtrl, camera, platform, file, filePath, transfer, storage, toastCtrl, modalCtrl, formBuilder, loadingCtrl, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.platform = platform;
        this.file = file;
        this.filePath = filePath;
        this.transfer = transfer;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.viewCtrl = viewCtrl;
        this.my_photo = null;
        this.images_list = new Array();
        this.obj = this.navParams.get('item');
        this.form = formBuilder.group({
            title: [this.obj.title, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].maxLength(200), __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required])],
            price: [this.obj.price, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            categories: [this.obj.categories, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            county: [this.obj.county, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            cities: [this.obj.cities, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            content: [this.obj.content, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            condition: [this.obj.condition, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            purpose: [this.obj.aim, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required]
        });
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.condition_lists = [
            { id: 0, name: 'new' },
            { id: 1, name: 'second hand' }
        ];
        this.purpose_lists = [
            { id: 0, name: 'Buy' },
            { id: 1, name: 'Sell' }
        ];
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/county_api/county").subscribe(function (data) {
            console.log(data.json());
            _this.county_lists = data.json();
        }, function (error) {
        });
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/categories_api/categories").subscribe(function (data) {
            console.log(data.json());
            _this.categories_lists = data.json();
        }, function (error) {
        });
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                console.log(obj);
                if (obj == null) {
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
                    modal.present();
                }
                else {
                    _this.user = obj;
                }
            });
        });
    }
    EditProductPage.prototype.load_cities = function () {
        var _this = this;
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/cities_api/cities_by_county_id?id=" + this.form.value.county).subscribe(function (data) {
            console.log(data.json());
            _this.cities_lists = data.json();
        }, function (error) {
        });
    };
    EditProductPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditProductPage');
    };
    EditProductPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewWillEnter PostProductPage');
        if (this.obj != null) {
            this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/images_api/images?product_id=" + this.obj.id).subscribe(function (data) {
                console.log(data.json());
                _this.images_list = data.json();
            }, function (error) {
            });
        }
    };
    EditProductPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    EditProductPage.prototype.edit = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var post_data = 'product_id=' + this.obj.id + '&title=' + this.form.value.title + '&content=' + this.form.value.content + '&county=' + this.form.value.county + '&cities=' + this.form.value.cities + '&price=' + this.form.value.price + '&condition=' + this.form.value.condition + '&purpose=' + this.form.value.purpose + '&categories=' + this.form.value.categories;
        this.http.post(this.base_url + 'api/products_api/update', post_data, { headers: headers }).subscribe(function (data) {
            var toast = _this.toastCtrl.create({
                message: 'Update Successfully',
                duration: 1000
            });
            toast.present();
        }, function (error) {
        });
    };
    EditProductPage.prototype.selectPhotoOptions = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Modify your album',
            buttons: [
                {
                    text: 'Take photo',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Select photo',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }
            ]
        });
        actionSheet.present();
    };
    EditProductPage.prototype.takePhoto = function (sourceType) {
        var _this = this;
        var camera_options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(camera_options).then(function (imagePath) {
            _this.my_photo = imagePath;
            console.log(imagePath);
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (error) {
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    EditProductPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.externalApplicationStorageDirectory, newFileName).then(function (success) {
            console.log(JSON.stringify(success));
            _this.my_photo = success.nativeURL;
            _this.uploadPhoto();
        }, function (error) {
        });
    };
    EditProductPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    EditProductPage.prototype.uploadPhoto = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Loading"
        });
        loader.present();
        var options = {
            fileKey: "file",
            fileName: this.my_photo,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: {
                'id': this.obj.id
            }
        };
        var fileTransfer = this.transfer.create();
        fileTransfer.upload(this.my_photo, __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'api/images_api/images', options).then(function (data) {
            console.log(JSON.stringify(data));
            loader.dismiss();
            _this.my_photo = null;
            var response = JSON.parse(data.response);
            _this.images_list.push(response);
        }, function (error) {
            console.log(error);
            loader.dismiss();
        });
    };
    return EditProductPage;
}());
EditProductPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-edit-product',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/edit-product/edit-product.html"*/'<ion-content>\n  <button ion-button icon-only clear class="btn-close" round (click)="dismiss()">\n    <ion-icon name="ios-close-circle-outline"></ion-icon>\n  </button>\n  <div padding>\n    <img class="logo" src="assets/img/logo_chili.png" alt="">\n    <h2 class="status">    {{\'edit_product\' | translate }}</h2>\n    <div padding>\n      <ion-grid class="gallery">\n        <ion-row *ngIf="images_list.length>0">\n          <ion-col col-6 *ngFor="let item of images_list">\n            <img src="{{base_url+item.path}}" />\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n    <div padding>\n      <ion-row>\n        <ion-col col-12>\n          <div class="wrapper-img" (click)="selectPhotoOptions()">\n            <div *ngIf="my_photo">\n              <img src="{{my_photo}}" style="width: 128px;height: 128px" />\n            </div>\n            <div *ngIf="!my_photo">\n              <img src="assets/img/thumb.png" />\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n    </div>\n  </div>\n\n  <form [formGroup]="form">\n  <div padding>\n    <ion-item>\n      <ion-label stacked>    {{\'title\' | translate }}</ion-label>\n      <ion-input type="text" formControlName="title" placeholder="Title"></ion-input>\n    </ion-item>\n     <p class="err"  *ngIf="!form.controls.title.valid  && (form.controls.title.dirty || submitAttempt)">*Require, More than 5 and less than 200 characters</p>\n\n    <ion-item>\n      <ion-label stacked>    {{\'price\' | translate }}</ion-label>\n      <ion-input type="number" formControlName="price" placeholder="Price"></ion-input>\n    </ion-item>\n      <p class="err"  *ngIf="!form.controls.price.valid  && (form.controls.price.dirty || submitAttempt)">* Required</p>\n\n    <ion-item>\n      <ion-label>    {{\'categories\' | translate }}</ion-label>\n      <ion-select formControlName="categories">\n        <ion-option *ngFor="let item of categories_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>    {{\'county\' | translate }}</ion-label>\n      <ion-select formControlName="county" (ionChange)="load_cities()">\n        <ion-option *ngFor="let item of county_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>    {{\'cities\' | translate }}</ion-label>\n      <ion-select formControlName="cities">\n        <ion-option *ngFor="let item of cities_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>    {{\'condition\' | translate }}</ion-label>\n      <ion-select formControlName="condition">\n        <ion-option *ngFor="let item of condition_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label>    {{\'purpose\' | translate }}</ion-label>\n      <ion-select formControlName="purpose">\n        <ion-option *ngFor="let item of purpose_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n  </div>\n  <div padding>\n    <ion-item>\n      <ion-label stacked>    {{\'content\' | translate }}</ion-label>\n      <ion-textarea formControlName="content" placeholder="Content"></ion-textarea>\n    </ion-item>\n       <p class="err"  *ngIf="!form.controls.content.valid  && (form.controls.content.dirty || submitAttempt)">* Required</p>\n    <button class="btn-save" ion-button round color="dark" type="submit" (click)="edit()" [disabled]="!form.valid">   \n     {{\'save\' | translate }}</button>\n  </div>\n  </form>\n\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/edit-product/edit-product.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__["a" /* FilePath */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_10__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
], EditProductPage);

//# sourceMappingURL=edit-product.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__post_product_post_product__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__post_detail_post_detail__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__setting_setting__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__profile_profile__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var FavoritesPage = (function () {
    function FavoritesPage(events, alertCtrl, toastCtrl, socialSharing, callNumber, navCtrl, http, storage, modalCtrl) {
        var _this = this;
        this.events = events;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.socialSharing = socialSharing;
        this.callNumber = callNumber;
        this.navCtrl = navCtrl;
        this.http = http;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        // this.url_favorites=Constant.domainConfig.favorites_api;
        this.lists = new Array();
        this.events.subscribe('user: change', function () {
            _this.storage.ready().then(function () {
                _this.storage.get('user').then(function (obj) {
                    console.log(obj);
                    if (obj == null) {
                        _this.user_id = null;
                        _this.lists = new Array();
                    }
                    else {
                        _this.ionViewWillEnter();
                    }
                });
            });
        });
    }
    FavoritesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FavoritesPage');
    };
    FavoritesPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewWillEnter TabsPage');
        this.first = -5;
        this.lists = new Array();
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                console.log(obj);
                if (obj == null) {
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
                    modal.present();
                }
                else {
                    _this.user_id = obj.id;
                    _this.loadMore();
                }
            });
        });
    };
    FavoritesPage.prototype.loadMore = function (infiniteScroll) {
        var _this = this;
        if (infiniteScroll === void 0) { infiniteScroll = null; }
        this.first += 5;
        if (this.user_id != null) {
            this.http.get(this.base_url + 'api/favoriest_api/favorites?first=' + this.first + '&offset=5' + '&user_id=' + this.user_id).subscribe(function (data) {
                console.log(data.json());
                var jsonData = data.json();
                for (var i = 0; i < jsonData.length; i++) {
                    _this.lists.push(jsonData[i]);
                }
                if (infiniteScroll) {
                    infiniteScroll.complete();
                }
            }, function (error) {
                if (infiniteScroll != null) {
                    infiniteScroll.enable(false);
                }
            });
        }
    };
    FavoritesPage.prototype.setting = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__setting_setting__["a" /* SettingPage */]);
        modal.present();
    };
    FavoritesPage.prototype.report = function (product_id, i) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/products_api/report', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.lists[i].report = data.json().report;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    FavoritesPage.prototype.call = function (phone) {
        this.callNumber.callNumber(phone, true)
            .then(function () { return console.log('Launched dialer!'); })
            .catch(function () { return console.log('Error launching dialer'); });
    };
    FavoritesPage.prototype.share = function (item) {
        this.socialSharing.share(item.name, item.content, null, __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'product?id=' + item.id);
    };
    FavoritesPage.prototype.up_product = function () {
        if (this.user_id != null && this.user_id != 0) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__post_product_post_product__["a" /* PostProductPage */]);
            modal.present();
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    FavoritesPage.prototype.favorites = function (product_id, i) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        this.http.post(this.base_url + 'api/favoriest_api/add_un_favorites', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
            console.log(data.json());
            if (data.json().favorites == false) {
                _this.lists.splice(i, 1);
            }
        }, function (error) {
        });
    };
    FavoritesPage.prototype.view = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__post_detail_post_detail__["a" /* PostDetailPage */], { item: item });
    };
    FavoritesPage.prototype.view_prf = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__profile_profile__["a" /* ProfilePage */], { user_id: id });
    };
    FavoritesPage.prototype.mail = function (item) {
        var _this = this;
        console.log(item);
        if (this.user_id == null) {
            //go to login page
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
            return;
        }
        var promtDialog = this.alertCtrl.create({
            title: 'Messages',
            inputs: [
                {
                    name: 'message',
                    placeholder: 'Message'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        ///send message via email system
                        _this.storage.ready().then(function () {
                            _this.storage.get('user').then(function (obj) {
                                if (obj.email == item.email) {
                                    var alert_1 = _this.alertCtrl.create({
                                        message: "you cannot send mail to yourself",
                                        buttons: ['Dismiss']
                                    });
                                    alert_1.present();
                                    return;
                                }
                                var post_data = 'email=' + item.email + '&message=' + data.message + '&user_name=' + obj.user_name + '&reply_to=' + obj.email;
                                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                });
                                _this.http.post(_this.base_url + 'api/users_api/send_enquiry', post_data, { headers: headers }).subscribe(function (data) {
                                    if (data.json().success == 0) {
                                        var toast = _this.toastCtrl.create({
                                            message: 'you just can send enquiry after 120 seconds',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                    else {
                                        var toast = _this.toastCtrl.create({
                                            message: 'Your messages have been sent, thank you !!!',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                }, function (error) {
                                });
                            });
                        });
                    }
                }
            ]
        });
        promtDialog.present();
    };
    return FavoritesPage;
}());
FavoritesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-favorites',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/favorites/favorites.html"*/'<ion-header>\n	<ion-navbar>\n		<button clear small color="light" ion-button class="setting-nav" (click)="setting()">\n        	<ion-icon name="ios-pin-outline"></ion-icon>\n      	</button>\n      	<div class="brand">\n			<!-- <ion-title>Home</ion-title> -->\n			<img src="assets/img/logo_chili.png" alt="">\n		</div>\n      	<button clear small color="light" ion-button class="upload-nav" (click)="up_product()">\n        	<ion-icon name="ios-paper-plane-outline"></ion-icon>\n      	</button>\n	</ion-navbar>\n</ion-header>\n\n\n\n<ion-content>\n  <div *ngIf="lists.empty == 1 || lists == 0" class="empty_data">\n    <div>\n      <h4>No favorite products</h4>\n      <p>Quickly choose your own favorite products offline!</p>\n    </div>\n  </div>\n\n  <ion-list class="lst-products" *ngIf="lists.empty != 1">\n    <ion-card *ngFor="let item of lists ; let i = index">\n      <ion-item>\n        <ion-avatar item-start (click)="view_prf(item.user_id)">\n              <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')==-1" src="{{base_url + item.avt}}">\n        <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')!=-1" src="{{item.avt}}">\n        <img *ngIf="item.avt == null" src="assets/avt.png">\n\n        </ion-avatar>\n        <h2><b (click)="view_prf(item.user_id)">{{item.user_name}}</b> posted {{item.created_at}}</h2>\n        <div class="more">\n          <button [ngClass]="{\'active\' : (item.report == true)}" ion-button icon-left clear (click)="report(item.id, i)">\n            <ion-icon name="ios-flag-outline"></ion-icon>\n          </button>\n        </div>\n      </ion-item>\n      <img src="{{base_url + item.image_path}}" (click)="view(item)" />\n      <ion-card-content (click)="view(item)">\n        <ion-card-title>{{item.title}}</ion-card-title>\n        <div class="local">\n          <ion-icon name="md-locate"></ion-icon>\n          {{item.county_name + \' - \' + item.cities_name}}\n        </div>\n        <p class="price">price: {{item.price}}</p>\n        <p>{{item.content.slice(0,107) + \'...\'}}</p>\n      </ion-card-content>\n      <ion-row>\n          <ion-col col-3>\n             <button ion-button icon-left clear (click)="mail(item)">\n            <ion-icon name="ios-mail"></ion-icon>\n          </button>\n          </ion-col>\n\n          <ion-col col-3>\n            <button ion-button icon-left clear (click)="call(item.phone)">\n              <ion-icon name="ios-call" ></ion-icon>\n            </button>\n          </ion-col>\n          \n          <ion-col col-3>\n            <button [ngClass]="{\'active\' : (item.favorites == true)}" ion-button icon-left clear (click)="favorites(item.product_id, i)">\n              <ion-icon name="ios-heart" ></ion-icon>\n            </button>\n          </ion-col>\n\n          <ion-col col-3>\n            <button ion-button icon-left clear (click)="share(item)">\n              <ion-icon name="md-share" ></ion-icon>\n            </button>\n          </ion-col>\n        </ion-row>\n    </ion-card>\n  </ion-list>\n\n\n  <ion-infinite-scroll (ionInfinite)="loadMore($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/favorites/favorites.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
], FavoritesPage);

//# sourceMappingURL=favorites.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__post_product_post_product__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__setting_setting__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__categories_detail_categories_detail__ = __webpack_require__(230);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CategoriesPage = (function () {
    function CategoriesPage(events, storage, modalCtrl, navCtrl, navParams, http) {
        var _this = this;
        this.events = events;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.user_id = null;
        this.base_url = '';
        this.county = null;
        this.cities = null;
        this.old_index = null;
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.events.subscribe('local: change', function () {
            _this.ionViewWillEnter();
        });
        this.events.subscribe('user: change', function () {
            _this.ionViewWillEnter();
        });
    }
    CategoriesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad CategoriesPage');
        this.http.get(this.base_url + "api/categories_api/categories").subscribe(function (data) {
            console.log(data.json());
            _this.lists = data.json();
        }, function (error) {
        });
    };
    CategoriesPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewWillEnter TabHomePage');
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                console.log(obj);
                if (obj != null) {
                    _this.user_id = obj.id;
                }
                else {
                    _this.user_id = null;
                }
            });
            _this.storage.get('local').then(function (data) {
                console.log(data);
                if (data != null && data.all_local == false) {
                    _this.county = data.county;
                    _this.cities = data.cities;
                }
            });
        });
    };
    CategoriesPage.prototype.open_cat = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__categories_detail_categories_detail__["a" /* CategoriesDetailPage */], { cat: item });
    };
    CategoriesPage.prototype.tree_cat = function (i) {
        if (this.old_index != null && this.old_index != i) {
            this.lists[this.old_index].active = false;
        }
        if (this.lists[i].active == true) {
            this.lists[i].active = false;
        }
        else {
            this.lists[i].active = true;
        }
        this.old_index = i;
    };
    CategoriesPage.prototype.setting = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__setting_setting__["a" /* SettingPage */]);
        modal.present();
    };
    CategoriesPage.prototype.up_product = function () {
        if (this.user_id != null && this.user_id != 0) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__post_product_post_product__["a" /* PostProductPage */]);
            modal.present();
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    return CategoriesPage;
}());
CategoriesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-categories',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/categories/categories.html"*/'<ion-header>\n	<ion-navbar>\n		<button clear small color="light" ion-button class="setting-nav" (click)="setting()">\n        	<ion-icon name="ios-pin-outline"></ion-icon>\n      	</button>\n      	<div class="brand">\n			<!-- <ion-title>Home</ion-title> -->\n			<img src="assets/img/logo_chili.png" alt="">\n		</div>\n      	<button clear small color="light" ion-button class="upload-nav" (click)="up_product()">\n        	<ion-icon name="ios-paper-plane-outline"></ion-icon>\n      	</button>\n	</ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  	<ion-card *ngFor="let item of lists, let i = index" [ngClass]="{\'show\' : (item.parent_id == 0), \'hidden\' : (item.parent_id != 0)}">\n  		<div [ngClass]="{\'active\' : (item.active == true)}" *ngIf="item.parent_id == 0" class="wrapper">\n	  		\n	  		<div class="thumb">\n			  	<img (click)="open_cat(item)" src="{{base_url + item.image}}"/>\n			  	<ion-card-content (click)="open_cat(item)">\n			    	<ion-card-title>{{item.name}}</ion-card-title>\n			    	<p><ion-icon name="md-stats"></ion-icon>{{item.total_item}} item</p>\n			  	</ion-card-content>\n			  	<button *ngIf="item.tree != false" clear small color="light" ion-button (click)="tree_cat(i)">\n		        	<ion-icon name="md-list"></ion-icon>\n		      	</button>\n			</div>\n		  	<ion-list class="tree" *ngIf="item.tree != false">\n			  	<button *ngFor="let item_tree of lists" [ngClass]="{\'show\' : (item_tree.parent_id == item.id), \'hidden\' : (item_tree.parent_id != item.id)}" ion-item (click)="open_cat(item_tree)">\n			  		<ion-label *ngIf="item_tree.parent_id == item.id">{{item_tree.name}}</ion-label>\n			  		<ion-badge item-end color="dark" *ngIf="item_tree.total_item != 0">{{item_tree.total_item}} item</ion-badge>\n			  	</button>\n			</ion-list>\n\n		</div>\n	</ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/categories/categories.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
], CategoriesPage);

//# sourceMappingURL=categories.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriesDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__post_product_post_product__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__post_detail_post_detail__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__setting_setting__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__profile_profile__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CategoriesDetailPage = (function () {
    function CategoriesDetailPage(socialSharing, callNumber, modalCtrl, events, storage, toastCtrl, alertCtrl, navCtrl, navParams, http) {
        var _this = this;
        this.socialSharing = socialSharing;
        this.callNumber = callNumber;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.user_id = null;
        this.county = null;
        this.cities = null;
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.lists = new Array();
        this.events.subscribe('local: change', function () {
            _this.ionViewWillEnter();
        });
        this.events.subscribe('user: change', function () {
            _this.ionViewWillEnter();
        });
    }
    CategoriesDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoriesDetailPage');
    };
    CategoriesDetailPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewWillEnter CategoriesDetailPage');
        var temp_cat = this.navParams.get('cat');
        this.first = -5;
        this.lists = new Array();
        this.cat_id = temp_cat.id;
        this.cat_name = temp_cat.name;
        this.county = null;
        this.cities = null;
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                console.log(obj);
                if (obj != null) {
                    _this.user_id = obj.id;
                }
                else {
                    _this.user_id = null;
                }
            });
            _this.storage.get('local').then(function (data) {
                console.log(data);
                if (data != null && data.all_local == false) {
                    _this.county = data.county;
                    _this.cities = data.cities;
                }
                _this.loadMore();
            });
        });
    };
    CategoriesDetailPage.prototype.loadMore = function (infiniteScroll) {
        var _this = this;
        if (infiniteScroll === void 0) { infiniteScroll = null; }
        this.first += 5;
        this.http.get(this.base_url + 'api/products_api/products?private=0&order=last&first=' + this.first + '&offset=10' + '&categories_id=' + this.cat_id + '&county_id=' + this.county + '&cities_id=' + this.cities + '&user_lg=' + this.user_id).subscribe(function (data) {
            console.log(data.json());
            var jsonData = data.json();
            for (var i = 0; i < jsonData.length; i++) {
                _this.lists.push(jsonData[i]);
            }
            if (infiniteScroll) {
                infiniteScroll.complete();
            }
        }, function (error) {
            if (infiniteScroll != null) {
                infiniteScroll.enable(false);
            }
        });
    };
    CategoriesDetailPage.prototype.setting = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__setting_setting__["a" /* SettingPage */]);
        modal.present();
    };
    CategoriesDetailPage.prototype.up_product = function () {
        if (this.user_id != null && this.user_id != 0) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__post_product_post_product__["a" /* PostProductPage */]);
            modal.present();
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    CategoriesDetailPage.prototype.report = function (product_id, i) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/products_api/report', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.lists[i].report = data.json().report;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    CategoriesDetailPage.prototype.call = function (phone) {
        this.callNumber.callNumber(phone, true)
            .then(function () { return console.log('Launched dialer!'); })
            .catch(function () { return console.log('Error launching dialer'); });
    };
    CategoriesDetailPage.prototype.share = function (item) {
        this.socialSharing.share(item.name, item.content, null, this.base_url + 'product?id=' + item.id);
    };
    CategoriesDetailPage.prototype.favorites = function (product_id, i) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/favoriest_api/add_un_favorites', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.lists[i].favorites = data.json().favorites;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    CategoriesDetailPage.prototype.mail = function (item) {
        var _this = this;
        console.log(item);
        if (this.user_id == null) {
            //go to login page
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
            return;
        }
        var promtDialog = this.alertCtrl.create({
            title: 'Messages',
            inputs: [
                {
                    name: 'message',
                    placeholder: 'Message'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        ///send message via email system
                        _this.storage.ready().then(function () {
                            _this.storage.get('user').then(function (obj) {
                                if (obj.email == item.email) {
                                    var alert_1 = _this.alertCtrl.create({
                                        message: "you cannot send mail to yourself",
                                        buttons: ['Dismiss']
                                    });
                                    alert_1.present();
                                    return;
                                }
                                var post_data = 'email=' + item.email + '&message=' + data.message + '&user_name=' + obj.user_name + '&reply_to=' + obj.email;
                                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                });
                                _this.http.post(_this.base_url + 'api/users_api/send_enquiry', post_data, { headers: headers }).subscribe(function (data) {
                                    if (data.json().success == 0) {
                                        var toast = _this.toastCtrl.create({
                                            message: 'you just can send enquiry after 120 seconds',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                    else {
                                        var toast = _this.toastCtrl.create({
                                            message: 'Your messages have been sent, thank you !!!',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                }, function (error) {
                                });
                            });
                        });
                    }
                }
            ]
        });
        promtDialog.present();
    };
    CategoriesDetailPage.prototype.view = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__post_detail_post_detail__["a" /* PostDetailPage */], { item: item });
    };
    CategoriesDetailPage.prototype.view_prf = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__profile_profile__["a" /* ProfilePage */], { user_id: id });
    };
    return CategoriesDetailPage;
}());
CategoriesDetailPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-categories-detail',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/categories-detail/categories-detail.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{cat_name}}</ion-title>\n    <div>\n      <button clear small color="light" ion-button class="setting-nav" (click)="setting()">\n        <ion-icon name="ios-pin-outline"></ion-icon>\n      </button>\n      <button clear small color="light" ion-button class="upload-nav" (click)="up_product()">\n        <ion-icon name="ios-paper-plane-outline"></ion-icon>\n      </button>\n    </div>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="lists.empty == 1" class="empty_data">\n    <div>\n      <h4>not found</h4>\n      <p>There are currently no products available in the area you require!</p>\n    </div>\n  </div>\n  <ion-list class="lst-products" *ngIf="lists.empty != 1">\n    <ion-card *ngFor="let item of lists ; let i = index">\n      <ion-item>\n        <ion-avatar item-start (click)="view_prf(item.user_id)">\n              <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')==-1" src="{{base_url + item.avt}}">\n        <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')!=-1" src="{{item.avt}}">\n        <img *ngIf="item.avt == null" src="assets/avt.png">\n\n        </ion-avatar>\n        <h2><b (click)="view_prf(item.user_id)">{{item.user_name}}</b> posted {{item.created_at}}</h2>\n        <div class="more">\n          <button [ngClass]="{\'active\' : (item.report == true)}" ion-button icon-left clear (click)="report(item.id, i)">\n            <ion-icon name="ios-flag-outline"></ion-icon>\n          </button>\n        </div>\n      </ion-item>\n      <img src="{{base_url + item.image_path}}" (click)="view(item)" />\n      <ion-card-content (click)="view(item)">\n        <ion-card-title>{{item.title}}</ion-card-title>\n        <div class="local">\n          <ion-icon name="md-locate"></ion-icon>\n          {{item.county_name + \' - \' + item.cities_name}}\n        </div>\n        <p class="price">price: {{item.price}}</p>\n        <p>{{item.content.slice(0,107) + \'...\'}}</p>\n      </ion-card-content>\n      <ion-row>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="mail(item)">\n            <ion-icon name="ios-mail"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="call(item.phone)">\n            <ion-icon name="ios-call"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button [ngClass]="{\'active\' : (item.favorites == true)}" ion-button icon-left clear (click)="favorites(item.id, i)">\n            <ion-icon name="ios-heart"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="share(item)">\n            <ion-icon name="md-share"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)="loadMore($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/categories-detail/categories-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
], CategoriesDetailPage);

//# sourceMappingURL=categories-detail.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__post_product_post_product__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__post_detail_post_detail__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__setting_setting__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__profile_profile__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var SearchPage = (function () {
    function SearchPage(socialSharing, callNumber, events, navCtrl, navParams, http, storage, toastCtrl, alertCtrl, modalCtrl) {
        var _this = this;
        this.socialSharing = socialSharing;
        this.callNumber = callNumber;
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.base_url = '';
        this.user_id = null;
        this.purpose = '';
        this.cat = '';
        this.county = null;
        this.cities = null;
        this.query = null;
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.lists = new Array();
        this.purpose_lists = [
            { id: 0, name: 'buy' },
            { id: 1, name: 'sell' }
        ];
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/categories_api/categories").subscribe(function (data) {
            console.log(data.json());
            _this.cat_lists = data.json();
        }, function (error) {
        });
        this.price = {
            'lower': 1,
            'upper': 600
        };
        this.events.subscribe('local: change', function () {
            _this.ionViewWillEnter();
            if (_this.query != null && _this.query != '') {
                _this.loadMore();
            }
        });
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    SearchPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.first = -5;
        this.lists = new Array();
        this.county = null;
        this.cities = null;
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                console.log("FICK" + obj);
                if (obj != null) {
                    _this.user_id = obj.id;
                }
                else {
                    _this.user_id = null;
                }
            });
            _this.storage.get('local').then(function (data) {
                console.log(data);
                if (data != null && data.all_local == false) {
                    _this.county = data.county;
                    _this.cities = data.cities;
                }
                if (_this.query != null && _this.query != '' && _this.query != ' ') {
                    _this.loadMore();
                }
            });
        });
    };
    SearchPage.prototype.onInput = function (event) {
        console.log(event.target.value);
        this.query = event.target.value;
        this.lists = new Array();
        this.first = -5;
        this.loadMore();
    };
    SearchPage.prototype.select_change = function () {
        this.first = -5;
        if (this.query != null && this.query != '' && this.query != ' ') {
            this.lists = new Array();
            this.first = -5;
            this.loadMore();
        }
    };
    SearchPage.prototype.loadMore = function (infiniteScroll) {
        var _this = this;
        if (infiniteScroll === void 0) { infiniteScroll = null; }
        this.first += 5;
        if (this.query != null && this.query != '' && this.query != ' ') {
            this.http.get(this.base_url + 'api/products_api/products?private=0&order=last&first=' + this.first + '&offset=5' + '&title=' + this.query + '&min_price=' + this.price.lower + '&max_price=' + this.price.upper + '&aim=' + this.purpose + '&categories_id=' + this.cat + '&county_id=' + this.county + '&cities_id=' + this.cities).subscribe(function (data) {
                console.log(data.json());
                var jsonData = data.json();
                for (var i = 0; i < jsonData.length; i++) {
                    _this.lists.push(jsonData[i]);
                }
                console.log(_this.lists);
                if (infiniteScroll) {
                    infiniteScroll.complete();
                }
            }, function (error) {
                if (infiniteScroll != null) {
                    infiniteScroll.enable(false);
                }
            });
        }
    };
    SearchPage.prototype.setting = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__setting_setting__["a" /* SettingPage */]);
        modal.present();
    };
    SearchPage.prototype.up_product = function () {
        if (this.user_id != null && this.user_id != 0) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__post_product_post_product__["a" /* PostProductPage */]);
            modal.present();
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    SearchPage.prototype.report = function (product_id, i) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/products_api/report', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.lists[i].report = data.json().report;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    SearchPage.prototype.call = function (phone) {
        this.callNumber.callNumber(phone, true)
            .then(function () { return console.log('Launched dialer!'); })
            .catch(function () { return console.log('Error launching dialer'); });
    };
    SearchPage.prototype.share = function (item) {
        this.socialSharing.share(item.name, item.content, null, __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'product?id=' + item.id);
    };
    SearchPage.prototype.favorites = function (product_id, i) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/favoriest_api/add_un_favorites', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.lists[i].favorites = data.json().favorites;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    SearchPage.prototype.mail = function (item) {
        var _this = this;
        console.log(item);
        if (this.user_id == null) {
            //go to login page
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
            modal.present();
            return;
        }
        var promtDialog = this.alertCtrl.create({
            title: 'Messages',
            inputs: [
                {
                    name: 'message',
                    placeholder: 'Message'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        ///send message via email system
                        _this.storage.ready().then(function () {
                            _this.storage.get('user').then(function (obj) {
                                if (obj.email == item.email) {
                                    var alert_1 = _this.alertCtrl.create({
                                        message: "you cannot send mail to yourself",
                                        buttons: ['Dismiss']
                                    });
                                    alert_1.present();
                                    return;
                                }
                                var post_data = 'email=' + item.email + '&message=' + data.message + '&user_name=' + obj.user_name + '&reply_to=' + obj.email;
                                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                });
                                _this.http.post(_this.base_url + 'api/users_api/send_enquiry', post_data, { headers: headers }).subscribe(function (data) {
                                    if (data.json().success == 0) {
                                        var toast = _this.toastCtrl.create({
                                            message: 'you just can send enquiry after 120 seconds',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                    else {
                                        var toast = _this.toastCtrl.create({
                                            message: 'Your messages have been sent, thank you !!!',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                }, function (error) {
                                });
                            });
                        });
                    }
                }
            ]
        });
        promtDialog.present();
    };
    SearchPage.prototype.view = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__post_detail_post_detail__["a" /* PostDetailPage */], { item: item });
    };
    SearchPage.prototype.view_prf = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__profile_profile__["a" /* ProfilePage */], { user_id: id });
    };
    return SearchPage;
}());
SearchPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-search',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/search/search.html"*/'<ion-header>\n  <ion-navbar>\n    <button clear small color="light" ion-button class="setting-nav" (click)="setting()">\n      <ion-icon name="ios-pin-outline"></ion-icon>\n    </button>\n    <div class="brand">\n      <!-- <ion-title>Home</ion-title> -->\n      <img src="assets/img/logo_chili.png" alt="">\n    </div>\n    <button clear small color="light" ion-button class="upload-nav" (click)="up_product()">\n      <ion-icon name="ios-paper-plane-outline"></ion-icon>\n    </button>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="wrapper-query">\n    <ion-row>\n      <ion-col col-6>\n        <ion-item>\n          <ion-label style="max-width: 30px;">\n            <ion-icon name="ios-swap-outline"></ion-icon>\n          </ion-label>\n          <ion-select style="flex: 1; max-width: none; padding-left: 0; text-align: center; margin: 0" [(ngModel)]="purpose" (ionChange)="select_change()">\n            <ion-option value="">All purpose</ion-option>\n            <ion-option *ngFor="let item of purpose_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n      <ion-col col-6>\n        <ion-item>\n          <ion-label style="max-width: 30px;">\n            <ion-icon name="ios-list-outline"></ion-icon>\n          </ion-label>\n          <ion-select style="flex: 1; max-width: none; padding-left: 0; text-align: center; margin: 0" [(ngModel)]="cat" (ionChange)="select_change()">\n            <ion-option value="">All Categories</ion-option>\n            <ion-option *ngFor="let item of cat_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row class="price">\n      <ion-item>\n        <ion-label style="width: 100%; margin-bottom: 0; margin-right: 5px;">Price\n          <span style="float: right;">\n					    <ion-badge style="margin: 0;" color="dark" item-end="">{{price.lower}}</ion-badge>\n					    <ion-badge style="margin: 0;" color="dark" item-end="">{{price.upper}}</ion-badge>\n				    </span>\n        </ion-label>\n        <ion-range style="padding: 0;" dualKnobs="true" color="dark" [(ngModel)]="price" min="0" max="1000" step="5" (ionChange)="select_change()">\n          <ion-label small range-left>0</ion-label>\n          <ion-label small range-right>1000</ion-label>\n        </ion-range>\n      </ion-item>\n    </ion-row>\n    <ion-row>\n      <ion-searchbar [(ngModel)]="myInput" [showCancelButton]="shouldShowCancel" (ionInput)="onInput($event)" (ionCancel)="onCancel($event)"></ion-searchbar>\n    </ion-row>\n  </div>\n  <div *ngIf="lists[0] == null && query != null" class="empty_data">\n    <div>\n      <h4>not found</h4>\n      <p>Did not find the right product for your search!</p>\n    </div>\n  </div>\n  <ion-list class="lst-products" *ngIf="lists[0] != null">\n    <ion-card *ngFor="let item of lists ; let i = index">\n      <ion-item>\n        <ion-avatar item-start (click)="view_prf(item.user_id)">\n              <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')==-1" src="{{base_url + item.avt}}">\n        <img *ngIf="item.avt != null && item.avt.indexOf(\'graph.facebook.com\')!=-1" src="{{item.avt}}">\n        <img *ngIf="item.avt == null" src="assets/avt.png">\n\n        </ion-avatar>\n        <h2><b (click)="view_prf(item.user_id)">{{item.user_name}}</b> posted {{item.created_at}}</h2>\n        <div class="more">\n          <button [ngClass]="{\'active\' : (item.report == true)}" ion-button icon-left clear (click)="report(item.id, i)">\n            <ion-icon name="ios-flag-outline"></ion-icon>\n          </button>\n        </div>\n      </ion-item>\n      <img src="{{base_url + item.image_path}}" (click)="view(item)" />\n      <ion-card-content (click)="view(item)">\n        <ion-card-title>{{item.title}}</ion-card-title>\n        <div class="local">\n          <ion-icon name="md-locate"></ion-icon>\n          {{item.county_name + \' - \' + item.cities_name}}\n        </div>\n        <p class="price">price: {{item.price}}</p>\n        <p>{{item.content.slice(0,107) + \'...\'}}</p>\n      </ion-card-content>\n      <ion-row>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="mail(item)">\n            <ion-icon name="ios-mail"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="call(item.phone)">\n            <ion-icon name="ios-call"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button [ngClass]="{\'active\' : (item.favorites == true)}" ion-button icon-left clear (click)="favorites(item.id, i)">\n            <ion-icon name="ios-heart"></ion-icon>\n          </button>\n        </ion-col>\n        <ion-col col-3>\n          <button ion-button icon-left clear (click)="share(item)">\n            <ion-icon name="md-share"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)="loadMore($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/search/search.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
], SearchPage);

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(248);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_about_about__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_search_search__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_favorites_favorites__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_categories_categories__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_signup_signup__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_setting_setting__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_post_product_post_product__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_post_detail_post_detail__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_categories_detail_categories_detail__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_tabs_tabs__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_activation_code_activation_code__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_edit_product_edit_product__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_modal_rating_modal_rating__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_status_bar__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_facebook__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_transfer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_file_path__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_camera__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_onesignal__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ngx_translate_core__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ngx_translate_http_loader__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34_ionic2_rating__ = __webpack_require__(321);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
































/*translate loader*/


/*end translate loader*/
function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_33__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}

var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_search_search__["a" /* SearchPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_favorites_favorites__["a" /* FavoritesPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_categories_categories__["a" /* CategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_setting_setting__["a" /* SettingPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_post_product_post_product__["a" /* PostProductPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_post_detail_post_detail__["a" /* PostDetailPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_categories_detail_categories_detail__["a" /* CategoriesDetailPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_activation_code_activation_code__["a" /* ActivationCodePage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_edit_product_edit_product__["a" /* EditProductPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_modal_rating_modal_rating__["a" /* ModalRatingPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_34_ionic2_rating__["a" /* Ionic2RatingModule */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */]),
            __WEBPACK_IMPORTED_MODULE_32__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                loader: {
                    provide: __WEBPACK_IMPORTED_MODULE_32__ngx_translate_core__["a" /* TranslateLoader */],
                    useFactory: (createTranslateLoader),
                    deps: [__WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */]]
                }
            })
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_8__pages_about_about__["a" /* AboutPage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_search_search__["a" /* SearchPage */],
            __WEBPACK_IMPORTED_MODULE_11__pages_favorites_favorites__["a" /* FavoritesPage */],
            __WEBPACK_IMPORTED_MODULE_12__pages_categories_categories__["a" /* CategoriesPage */],
            __WEBPACK_IMPORTED_MODULE_13__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_signup_signup__["a" /* SignupPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_setting_setting__["a" /* SettingPage */],
            __WEBPACK_IMPORTED_MODULE_17__pages_post_product_post_product__["a" /* PostProductPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_post_detail_post_detail__["a" /* PostDetailPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_categories_detail_categories_detail__["a" /* CategoriesDetailPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_activation_code_activation_code__["a" /* ActivationCodePage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_edit_product_edit_product__["a" /* EditProductPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_tabs_tabs__["a" /* TabsPage */],
            __WEBPACK_IMPORTED_MODULE_23__pages_modal_rating_modal_rating__["a" /* ModalRatingPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_24__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_30__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_25__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_29__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_28__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_26__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_31__ionic_native_onesignal__["a" /* OneSignal */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(222);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import {TranslateService} from '@ngx-translate/core';

var MyApp = (function () {
    // translate: TranslateService,
    function MyApp(platform, statusBar, translate, oneSignal, splashScreen) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.translate = translate;
        this.oneSignal = oneSignal;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            translate.setDefaultLang('en');
            _this.initializeApp();
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            if (!_this.platform.is('mobileweb') && !_this.platform.is('core')) {
                _this.oneSignal.startInit(__WEBPACK_IMPORTED_MODULE_6__config_constants__["e" /* onesignal_app_id */], __WEBPACK_IMPORTED_MODULE_6__config_constants__["d" /* google_project_number */]);
                _this.oneSignal.inFocusDisplaying(_this.oneSignal.OSInFocusDisplayOption.InAppAlert);
                _this.oneSignal.handleNotificationReceived().subscribe(function () {
                    // do something when notification is received
                });
                _this.oneSignal.handleNotificationOpened().subscribe(function () {
                    // do something when a notification is opened
                });
                _this.oneSignal.endInit();
            }
        });
    };
    return MyApp;
}());
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__["a" /* OneSignal */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (control) {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { "invalidEmail": true };
        }
        return null;
    };
    return EmailValidator;
}());

//# sourceMappingURL=email.js.map

/***/ }),

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhoneValidator; });
var PhoneValidator = (function () {
    function PhoneValidator() {
    }
    PhoneValidator.isValid = function (control) {
        var regExp = /^[0-9]{10}$/;
        if (!regExp.test(control.value)) {
            return { "invalidMobile": true };
        }
        return null;
    };
    return PhoneValidator;
}());

//# sourceMappingURL=phone.js.map

/***/ }),

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserNameValidator; });
var UserNameValidator = (function () {
    function UserNameValidator() {
    }
    UserNameValidator.isValid = function (control) {
        var regExp = /^[a-zA-Z0-9.\-_$@*!]{3,30}$/;
        if (!regExp.test(control.value)) {
            return { "invalidUserName": true };
        }
        return null;
    };
    return UserNameValidator;
}());

//# sourceMappingURL=username.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordValidator; });
var PasswordValidator = (function () {
    function PasswordValidator() {
    }
    PasswordValidator.isMatch = function (control) {
        if (control.value == control.root.value['pwd']) {
            console.log('passwords  match');
            return null;
        }
        else {
            return { 'notMatch': true };
        }
    };
    return PasswordValidator;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the About page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AboutPage = (function () {
    function AboutPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AboutPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AboutPage');
    };
    return AboutPage;
}());
AboutPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-about',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/about/about.html"*/'<ion-header>\n	<ion-navbar>\n		<button ion-button menuToggle>\n  			<ion-icon name="menu"></ion-icon>\n		</button>\n		<ion-title>About</ion-title>\n	</ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/about/about.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], AboutPage);

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivationCodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ActivationCodePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ActivationCodePage = (function () {
    function ActivationCodePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ActivationCodePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ActivationCodePage');
    };
    return ActivationCodePage;
}());
ActivationCodePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-activation-code',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/activation-code/activation-code.html"*/'<!--\n  Generated template for the ActivationCodePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>activation_code</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/activation-code/activation-code.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
], ActivationCodePage);

//# sourceMappingURL=activation-code.js.map

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__post_detail_post_detail__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__post_product_post_product__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__setting_setting__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__edit_product_edit_product__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_transfer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_path__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__ = __webpack_require__(67);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















/**
 * Generated class for the Profile page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ProfilePage = (function () {
    function ProfilePage(navParams, navCtrl, events, modalCtrl, actionSheetCtrl, camera, platform, file, filePath, loadingCtrl, transfer, http, storage) {
        var _this = this;
        this.navParams = navParams;
        this.navCtrl = navCtrl;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.platform = platform;
        this.file = file;
        this.filePath = filePath;
        this.loadingCtrl = loadingCtrl;
        this.transfer = transfer;
        this.http = http;
        this.storage = storage;
        this.base_url = '';
        this.obj = '';
        this.user_id = null;
        this.user_view = null;
        this.total_item = 0;
        this.check_edit = false;
        this.my_photo = null;
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.obj = new Array();
        this.lists = new Array();
        this.events.subscribe('user: change', function () {
            _this.msg_err_edit = null;
            _this.msg_err_pwd = null;
            _this.old_pwd = null;
            _this.new_pwd = null;
            _this.confirm_pwd = null;
            _this.ionViewWillEnter();
        });
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewWillEnter ProfilePage');
        this.obj = new Array();
        this.first = -8;
        this.lists = new Array();
        this.user_view = this.navParams.get('user_id');
        if (this.user_view != null) {
            this.http.get(this.base_url + 'api/users_api/user?id=' + this.user_view).subscribe(function (data) {
                console.log(data.json());
                _this.obj = data.json()[0];
                _this.http.get(_this.base_url + 'api/products_api/products?private=0&order=last&first=0' + '&offset=1000' + '&user_id=' + _this.obj.id).subscribe(function (data) {
                    if (data.json().length > 0) {
                        _this.total_item = data.json().length;
                    }
                }, function (error) { });
                _this.storage.ready().then(function () {
                    _this.storage.get('user').then(function (obj) {
                        console.log(obj);
                        if (obj != null) {
                            _this.user_id = obj.id;
                        }
                    });
                });
                _this.loadMore();
            }, function (error) {
            });
        }
        else {
            this.storage.ready().then(function () {
                _this.storage.get('user').then(function (obj) {
                    console.log(obj);
                    if (obj != null) {
                        _this.obj = obj;
                        _this.user_id = obj.id;
                        _this.full_name = obj.full_name;
                        _this.phone = obj.phone;
                        _this.address = obj.address;
                        _this.http.get(_this.base_url + 'api/products_api/products?private=0&order=last&first=0' + '&offset=1000' + '&user_id=' + _this.obj.id).subscribe(function (data) {
                            if (data.json().length > 0) {
                                _this.total_item = data.json().length;
                            }
                        }, function (error) { });
                        _this.loadMore();
                    }
                });
            });
        }
    };
    ProfilePage.prototype.changeAvt = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Waitting"
        });
        loader.present();
        var options = {
            fileKey: "avt",
            fileName: this.my_photo,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: {
                'user_id': this.user_id,
            }
        };
        var fileTransfer = this.transfer.create();
        fileTransfer.upload(this.my_photo, __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'api/users_api/update_avt', options).then(function (data) {
            loader.dismiss();
            _this.my_photo = null;
            var response = JSON.parse(data.response);
            _this.obj.avt = response.avt;
            _this.storage.set('user', _this.obj);
        }, function (error) {
            console.log(error);
            loader.dismiss();
        });
    };
    ProfilePage.prototype.selectPhotoOptions = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Modify your album',
            buttons: [
                {
                    text: 'Take photo',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Select photo',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ProfilePage.prototype.takePhoto = function (sourceType) {
        var _this = this;
        var camera_options = {
            quality: 75,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(camera_options).then(function (imagePath) {
            _this.my_photo = imagePath;
            console.log(imagePath);
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (error) {
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    ProfilePage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.externalApplicationStorageDirectory, newFileName).then(function (success) {
            console.log(JSON.stringify(success));
            _this.my_photo = success.nativeURL;
            _this.changeAvt();
        }, function (error) {
        });
    };
    ProfilePage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    ProfilePage.prototype.loadMore = function (infiniteScroll) {
        var _this = this;
        if (infiniteScroll === void 0) { infiniteScroll = null; }
        this.first += 8;
        this.http.get(this.base_url + 'api/products_api/products?private=0&order=last&first=' + this.first + '&offset=8' + '&user_id=' + this.obj.id + '&user_lg=' + this.user_id).subscribe(function (data) {
            console.log(data.json());
            var jsonData = data.json();
            for (var i = 0; i < jsonData.length; i++) {
                _this.lists.push(jsonData[i]);
            }
            if (infiniteScroll) {
                infiniteScroll.complete();
            }
        }, function (error) {
            if (infiniteScroll != null) {
                infiniteScroll.enable(false);
            }
        });
    };
    ProfilePage.prototype.setting = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__setting_setting__["a" /* SettingPage */]);
        modal.present();
    };
    ProfilePage.prototype.up_product = function () {
        if (this.user_id != null && this.user_id != 0) {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__post_product_post_product__["a" /* PostProductPage */]);
            modal.present();
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    ProfilePage.prototype.view = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__post_detail_post_detail__["a" /* PostDetailPage */], { item: item });
    };
    ProfilePage.prototype.edit_prf = function () {
        this.check_edit = true;
    };
    ProfilePage.prototype.un_edit_prf = function () {
        this.check_edit = false;
    };
    ProfilePage.prototype.edit_product = function (item) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__edit_product_edit_product__["a" /* EditProductPage */], { item: item });
        modal.present();
    };
    ProfilePage.prototype.upload_edit = function () {
        var _this = this;
        var reg = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
        if (this.full_name.length >= 5 &&
            this.full_name.length <= 60 &&
            reg.test(this.phone) == true &&
            this.address.length >= 5 &&
            this.address.length <= 250) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/users_api/update', 'id=' + this.user_id + '&full_name=' + this.full_name + '&phone=' + this.phone + '&address=' + this.address, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                if (data.json().ok == 0) {
                    _this.msg_err_edit = 'You enter a missing or incorrect input 2';
                }
                else {
                    console.log(data.json());
                    var user = data.json()[0];
                    _this.storage.set('user', user);
                    _this.events.publish('user: change');
                    _this.msg_err_edit = 'Update Profile successfully';
                }
            }, function (error) {
            });
        }
        else {
            this.msg_err_edit = 'You enter a missing or incorrect input 1';
        }
    };
    ProfilePage.prototype.change_password = function () {
        var _this = this;
        var reg = /^[a-zA-Z0-9]+$/;
        if (this.old_pwd != null && this.new_pwd != null && this.new_pwd == this.confirm_pwd && this.new_pwd.length <= 60 && this.new_pwd.length >= 5 && reg.test(this.new_pwd) == true) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/users_api/pwd', 'id=' + this.user_id + '&old_pass=' + this.old_pwd + '&new_pass=' + this.new_pwd, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                if (data.json().ok == 1) {
                    _this.msg_err_pwd = 'Change password successfully';
                    _this.old_pwd = null;
                    _this.new_pwd = null;
                    _this.confirm_pwd = null;
                }
                else {
                    _this.msg_err_pwd = 'Your username or password is wrong';
                }
            }, function (error) {
            });
        }
        else {
            this.msg_err_pwd = 'You enter a missing or incorrect input';
        }
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        this.storage.ready().then(function () {
            _this.storage.remove('user').then(function (success) {
                _this.events.publish('user: change');
                // this.navCtrl.parent.select(0);
            });
        });
    };
    ProfilePage.prototype.open_login = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
        modal.present();
    };
    ProfilePage.prototype.dismiss = function () {
        this.navCtrl.pop();
    };
    return ProfilePage;
}());
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-profile',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/profile/profile.html"*/'<ion-header [ngClass]="{\'hidden\' : (user_view != null)}">\n  <ion-navbar>\n    <button *ngIf="user_view == null" clear small color="light" ion-button class="setting-nav" (click)="setting()">\n      <ion-icon name="ios-pin-outline"></ion-icon>\n    </button>\n    <div *ngIf="user_view == null" class="brand">\n      <!-- <ion-title>Home</ion-title> -->\n      <img src="assets/img/logo_chili.png" alt="">\n    </div>\n    <button *ngIf="user_view == null" clear small color="light" ion-button class="upload-nav" (click)="up_product()">\n      <ion-icon name="ios-paper-plane-outline"></ion-icon>\n    </button>\n  </ion-navbar>\n</ion-header>\n<ion-content [ngClass]="{\'no-header\' : (user_view != null)}" *ngIf="obj == null || obj == 0">\n  <div class="empty_data">\n    <div>\n      <h4>{{\'need_to_login\' | translate }}</h4>\n      <p> {{\'login_quote\' | translate }}</p>\n      <button small color="light" ion-button color="dark" round (click)="open_login()"> {{\'login_now\' | translate }}</button>\n    </div>\n  </div>\n</ion-content>\n<ion-content [ngClass]="{\'no-header\' : (user_view != null)}" *ngIf="obj != null && obj != 0">\n  <button ion-button color="light" class="btn-close" small clear *ngIf="user_view != null" (click)="dismiss()">\n    <ion-icon name="ios-close-circle-outline"></ion-icon>\n  </button>\n  <div padding class="main-info">\n    <div class="wrapper-info">\n      <button ion-button color="light" clear *ngIf="user_view == null" (click)="logout()">\n        <ion-icon name="md-log-out"></ion-icon>\n      </button>\n      <h3>{{obj.user_name}}</h3>\n\n      <span *ngIf="user_view == null">\n      <div class="avt" (click)=\'selectPhotoOptions()\'>\n        <img *ngIf="obj.avt != null && obj.avt.indexOf(\'graph.facebook.com\')==-1" src="{{base_url + obj.avt}}">\n        <img *ngIf="obj.avt != null && obj.avt.indexOf(\'graph.facebook.com\')!=-1" src="{{obj.avt}}">\n        <img *ngIf="obj.avt == null" src="assets/avt.png">\n        <span class="title">    {{\'tap_to_change_avt\' | translate }}</span>\n      </div>\n      </span>\n\n            <span *ngIf="user_view != null">\n      <div class="avt">\n        <img *ngIf="obj.avt != null && obj.avt.indexOf(\'graph.facebook.com\')==-1" src="{{base_url + obj.avt}}">\n        <img *ngIf="obj.avt != null && obj.avt.indexOf(\'graph.facebook.com\')!=-1" src="{{obj.avt}}">\n        <img *ngIf="obj.avt == null" src="assets/avt.png">\n        <span class="title">    {{\'tap_to_change_avt\' | translate }}</span>\n      </div>\n      </span>\n\n      <button ion-button color="light" clear *ngIf="user_view == null" (click)="edit_prf()">\n        <ion-icon name="md-create"></ion-icon>\n      </button>\n      <p class="email">{{obj.email}}</p>\n      <p class="phone">\n        <ion-icon name="ios-call"></ion-icon>+{{obj.phone}}</p>\n      <p class="photo_numb">All {{total_item}} items posted</p>\n    </div>\n  </div>\n  <div class="more-info" padding *ngIf="user_view != null">\n    <p> {{\'fullname\' | translate }}:</p>\n    <h5>{{obj.full_name}}</h5>\n    <p> {{\'address\' | translate }}:</p>\n    <h6>{{obj.address}}</h6>\n  </div>\n  <div class="wrapper-edit" padding *ngIf="user_view == null && check_edit == true">\n    <h4>{{\'edit_profile\' | translate }}</h4>\n    <ion-item>\n      <ion-label stacked> {{\'fullname\' | translate }}</ion-label>\n      <ion-input [(ngModel)]="full_name" type="text"></ion-input>\n    </ion-item>\n    <i class="text-left">* 5 < length < 50 characters</i>\n    <ion-item>\n      <ion-label stacked>Phone</ion-label>\n      <ion-input [(ngModel)]="phone" type="number"></ion-input>\n    </ion-item>\n    <i class="text-left">* 6 < length < 20 number</i>\n    <ion-item>\n      <ion-label stacked>Address</ion-label>\n      <ion-input [(ngModel)]="address" type="text"></ion-input>\n    </ion-item>\n    <i class="text-left">* 5 < length < 250 characters</i>\n    <b *ngIf="msg_err_edit != null" class="msg_err">{{msg_err_edit}}</b>\n    <button ion-button small clear (click)="upload_edit()">\n      {{\'save\' | translate }}\n    </button>\n    <h4>{{\'change_password\' | translate }}</h4>\n    <i>5 < pwd < 60 characters && do not contain special characters</i>\n    <ion-item>\n      <ion-label stacked> {{\'old_password\' | translate }}</ion-label>\n      <ion-input [(ngModel)]="old_pwd" type="password"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked> {{\'new_password\' | translate }}</ion-label>\n      <ion-input [(ngModel)]="new_pwd" type="password"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label stacked> {{\'confirm\' | translate }}</ion-label>\n      <ion-input [(ngModel)]="confirm_pwd" type="password"></ion-input>\n    </ion-item>\n    <b *ngIf="msg_err_pwd != null" class="msg_err">{{msg_err_pwd}}</b>\n    <button ion-button small clear (click)="change_password()">\n      {{\'save_password\' | translate }}\n    </button>\n    <div>\n      <button ion-button clear class="btn-close-edit" (click)="un_edit_prf()">\n        <ion-icon name="ios-arrow-dropup-outline"></ion-icon>\n      </button>\n    </div>\n  </div>\n  <ion-row *ngIf="lists.empty != 1">\n    <h4>{{\'all_products\' | translate }}</h4>\n    <ion-col col-6 *ngFor="let item of lists ; let i = index">\n      <ion-card>\n        <img src="{{base_url + item.image_path}}" (click)="view(item)" />\n        <ion-card-content (click)="view(item)">\n          <ion-card-title>{{item.title}}</ion-card-title>\n          <p class="price">{{\'price\' | translate }}: {{item.price}}</p>\n        </ion-card-content>\n        <button ion-button small clear *ngIf="user_view == null" (click)="edit_product(item)">\n          <ion-icon name="md-create"></ion-icon>\n        </button>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <ion-infinite-scroll (ionInfinite)="loadMore($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/profile/profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_13__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_11__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_path__["a" /* FilePath */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_10__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostProductPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the PostProduct page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PostProductPage = (function () {
    function PostProductPage(navCtrl, navParams, http, actionSheetCtrl, camera, platform, file, filePath, transfer, storage, modalCtrl, loadingCtrl, formBuilder, alertCtrl, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.platform = platform;
        this.file = file;
        this.filePath = filePath;
        this.transfer = transfer;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.viewCtrl = viewCtrl;
        this.my_photo = null;
        this.form = formBuilder.group({
            title: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].minLength(5), __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].maxLength(200), __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required])],
            price: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            categories: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            county: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            cities: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            content: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            condition: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            purpose: ['', __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required]
        });
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                console.log(obj);
                if (obj == null) {
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
                    modal.present();
                }
                else {
                    _this.user = obj;
                }
            });
        });
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.condition_lists = [
            { id: 0, name: 'new' },
            { id: 1, name: 'second hand' }
        ];
        this.purpose_lists = [
            { id: 0, name: 'Buy' },
            { id: 1, name: 'Sell' }
        ];
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/county_api/county").subscribe(function (data) {
            console.log(data.json());
            _this.county_lists = data.json();
        }, function (error) {
        });
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/categories_api/categories").subscribe(function (data) {
            console.log(data.json());
            _this.categories_lists = data.json();
        }, function (error) {
        });
    }
    PostProductPage.prototype.loadCities = function () {
        var _this = this;
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/cities_api/cities_by_county_id?id=" + this.form.value.county).subscribe(function (data) {
            console.log(data.json());
            _this.cities_lists = data.json();
        }, function (error) {
        });
    };
    PostProductPage.prototype.uploadPhoto = function () {
        var _this = this;
        if (this.my_photo == null) {
            var alert_1 = this.alertCtrl.create({
                message: 'Please choose a photo',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        var loader = this.loadingCtrl.create({
            content: "Loading"
        });
        loader.present();
        var options = {
            fileKey: "file",
            fileName: this.my_photo,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            params: {
                'categories': this.form.value.categories,
                'county': this.form.value.county,
                'cities': this.form.value.cities,
                'condition': this.form.value.condition,
                'purpose': this.form.value.purpose,
                'title': this.form.value.title,
                'price': this.form.value.price,
                'content': this.form.value.content,
                'fb_id': this.user.fb_id,
                'user_id': this.user.id
            }
        };
        var fileTransfer = this.transfer.create();
        fileTransfer.upload(this.my_photo, __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'api/products_api/products', options).then(function (data) {
            console.log(JSON.stringify(data));
            loader.dismiss();
            var alert = _this.alertCtrl.create({
                message: "Upload successfully",
                buttons: ['OK']
            });
            alert.present();
            _this.form.value.content = '';
            _this.form.value.title = '';
            _this.form.value.price = '';
            _this.my_photo = null;
        }, function (error) {
            console.log(error);
            loader.dismiss();
        });
    };
    PostProductPage.prototype.selectPhotoOptions = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Modify your album',
            buttons: [
                {
                    text: 'Take photo',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.CAMERA);
                    }
                }, {
                    text: 'Select photo',
                    handler: function () {
                        _this.takePhoto(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }
            ]
        });
        actionSheet.present();
    };
    PostProductPage.prototype.takePhoto = function (sourceType) {
        var _this = this;
        var camera_options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(camera_options).then(function (imagePath) {
            _this.my_photo = imagePath;
            console.log(imagePath);
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName());
            }
        }, function (error) {
            console.log("ERROR -> " + JSON.stringify(error));
        });
    };
    PostProductPage.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.externalApplicationStorageDirectory, newFileName).then(function (success) {
            console.log(JSON.stringify(success));
            _this.my_photo = success.nativeURL;
        }, function (error) {
        });
    };
    PostProductPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    PostProductPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PostProductPage');
    };
    PostProductPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return PostProductPage;
}());
PostProductPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-post-product',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/post-product/post-product.html"*/'\n<ion-content>\n  \n  <button ion-button icon-only clear class="btn-close" round (click)="dismiss()">\n    <ion-icon name="ios-close-circle-outline"></ion-icon>\n  </button>\n  \n\n  <form [formGroup]=\'form\' (ngSubmit)="uploadPhoto()">\n  <div padding>\n    <img class="logo" src="assets/img/logo_chili.png" alt="">\n    <h2 class="status">{{\'up_new_product\' | translate }}</h2>\n    <ion-item>\n      <ion-label stacked>{{\'title\' | translate }}</ion-label>\n      <ion-input type="text" formControlName="title" placeholder="Title"></ion-input>\n    </ion-item>\n     <p class="err"  *ngIf="!form.controls.title.valid  && (form.controls.title.dirty || submitAttempt)">*Require, More than 5 and less than 200 characters</p>\n    \n    <ion-item>\n      <ion-label stacked>{{\'price\' | translate }}</ion-label>\n      <ion-input type="number" formControlName="price" placeholder="Price"></ion-input>\n    </ion-item>\n     <p class="err"  *ngIf="!form.controls.price.valid  && (form.controls.price.dirty || submitAttempt)">* Required</p>\n  </div>\n\n  <div padding>\n    <ion-row>\n      <ion-col col-12>\n        <div class="wrapper-img" (click)="selectPhotoOptions()">\n          <div *ngIf="my_photo">\n            <img src="{{my_photo}}" style="width: 128px;height: 128px" />\n          </div>\n\n          <div *ngIf="!my_photo">\n            <img src="assets/img/thumb.png"/>\n          </div>\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n  \n  <div padding>\n    <ion-item>\n      <ion-label>    {{\'categories\' | translate }}</ion-label>\n      <ion-select formControlName="categories">\n        <ion-option *ngFor="let item of categories_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>    {{\'county\' | translate }}</ion-label>\n      <ion-select formControlName="county" (ionChange)="loadCities()">\n        <ion-option *ngFor="let item of county_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item>\n      <ion-label> {{\'cities\' | translate }}</ion-label>\n      <ion-select formControlName="cities">\n        <ion-option *ngFor="let item of cities_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item>\n      <ion-label> {{\'condition\' | translate }}</ion-label>\n      <ion-select formControlName="condition">\n        <ion-option *ngFor="let item of condition_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item>\n      <ion-label> {{\'purpose\' | translate }}</ion-label>\n      <ion-select formControlName="purpose">\n         <ion-option *ngFor="let item of purpose_lists; let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n      </ion-select>\n    </ion-item>\n  </div>\n\n  <div padding>\n    <ion-item>\n      <ion-label stacked>    {{\'content\' | translate }}</ion-label>\n      <ion-textarea formControlName="content" placeholder="Content"></ion-textarea>\n    </ion-item>\n    <p class="err"  *ngIf="!form.controls.content.valid  && (form.controls.content.dirty || submitAttempt)">* Required</p>\n    <button class="btn-save"  type="submit" ion-button round color="dark" [disabled]="!form.valid" >    {{\'add\' | translate }}</button>\n  </div>\n  </form>\n\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/post-product/post-product.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_path__["a" /* FilePath */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_10__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
], PostProductPage);

//# sourceMappingURL=post-product.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SettingPage = (function () {
    function SettingPage(events, navCtrl, http, storage, viewCtrl) {
        var _this = this;
        this.events = events;
        this.navCtrl = navCtrl;
        this.http = http;
        this.storage = storage;
        this.viewCtrl = viewCtrl;
        this.county = null;
        this.cities = null;
        this.all_local = false;
        this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/county_api/county").subscribe(function (data) {
            console.log(data.json());
            _this.county_list = data.json();
        }, function (error) {
        });
    }
    SettingPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.ready().then(function () {
            _this.storage.get('local').then(function (data) {
                console.log(data);
                if (data != null) {
                    _this.county = data.county;
                    _this.temp_county = data.county;
                    _this.cities = data.cities;
                    _this.all_local = data.all_local;
                    _this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/cities_api/cities_by_county_id?id=" + _this.county).subscribe(function (data) {
                        console.log(data.json());
                        if (data.json().empty == null) {
                            _this.cities_list = data.json();
                        }
                    });
                }
            });
        });
    };
    SettingPage.prototype.select_local = function () {
        var _this = this;
        if (this.county != this.temp_county) {
            this.http.get(__WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + "api/cities_api/cities_by_county_id?id=" + this.county).subscribe(function (data) {
                console.log(data.json());
                _this.cities_list = data.json();
                _this.temp_county = _this.county;
                _this.cities = null;
            });
        }
        this.storage.ready().then(function () {
            _this.storage.set('local', { 'county': _this.county, 'cities': _this.cities, 'all_local': _this.all_local });
            _this.events.publish('local: change');
        });
    };
    SettingPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return SettingPage;
}());
SettingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-setting',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/setting/setting.html"*/'\n\n<ion-content padding class="food-detail">\n\n  <ion-row class="close-modal" (click)="dismiss()"></ion-row>\n\n  <ion-row class="main-modal" col-2>\n\n    <div class="wrapper">\n\n      \n\n      <h6>{{\'set_your_location\' | translate}}</h6>\n\n      <p>{{\'set_location_quote\' | translate}}</p>\n\n      \n\n      <ion-item>\n\n        <ion-label>{{\'view_all_location\' | translate}}</ion-label>\n\n        <ion-toggle [(ngModel)]="all_local" (ionChange)="select_local()"></ion-toggle>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label>{{\'county\' | translate}}</ion-label>\n\n        <ion-select [disabled]="all_local ? true : null" [(ngModel)]="county" (ionChange)="select_local()">\n\n          <ion-option *ngFor="let item of county_list;let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label>{{\'cities\' | translate}}</ion-label>\n\n        <ion-select [disabled]="all_local ? true : null" [(ngModel)]="cities" (ionChange)="select_local()">\n\n          <ion-option value="null">All cities</ion-option>\n\n          <ion-option *ngFor="let item of cities_list;let i = index" value="{{item.id}}">{{item.name}}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n      <img src="assets/img/bg_setting.png">\n\n\n\n    </div>\n\n  </ion-row>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/setting/setting.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
], SettingPage);

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_constants__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__profile_profile__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modal_rating_modal_rating__ = __webpack_require__(226);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/**
 * Generated class for the Categories page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PostDetailPage = PostDetailPage_1 = (function () {
    function PostDetailPage(storage, events, modalCtrl, socialSharing, callNumber, navCtrl, navParams, alertCtrl, toastCtrl, http) {
        var _this = this;
        this.storage = storage;
        this.events = events;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.callNumber = callNumber;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.base_url = '';
        this.user_id = null;
        this.img_lists = new Array();
        this.base_url = __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url;
        this.obj = this.navParams.get('item');
        this.events.subscribe('local: change', function () {
            _this.ionViewWillEnter();
        });
        this.events.subscribe('user: change', function () {
            _this.ionViewWillEnter();
        });
        console.log(this.navParams.get('item'));
    }
    PostDetailPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PostDetailPage');
    };
    PostDetailPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewWillEnter PostDetailPage');
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                if (obj != null) {
                    _this.user_id = obj.id;
                }
            });
        });
        this.http.get(this.base_url + 'api/images_api/images?product_id=' + this.obj.id).subscribe(function (data) {
            console.log(data.json());
            _this.img_lists = data.json();
        }, function (error) {
        });
        this.http.get(this.base_url + 'api/products_api/products?private=0&order=last&first=0' + '&offset=4' + '&user_id=' + this.obj.user_id + '&user_lg=' + this.user_id).subscribe(function (data) {
            console.log(data.json());
            _this.attachment = data.json();
        }, function (error) { });
    };
    PostDetailPage.prototype.report = function (product_id) {
        var _this = this;
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/products_api/report', 'product_id=' + product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.obj.report = data.json().report;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    PostDetailPage.prototype.call = function (phone) {
        this.callNumber.callNumber(phone, true)
            .then(function () { return console.log('Launched dialer!'); })
            .catch(function () { return console.log('Error launching dialer'); });
    };
    PostDetailPage.prototype.share = function (item) {
        this.socialSharing.share(item.name, item.content, null, __WEBPACK_IMPORTED_MODULE_3__config_constants__["a" /* domainConfig */].base_url + 'product?id=' + item.id);
    };
    PostDetailPage.prototype.favorites = function () {
        var _this = this;
        var temp_product_id = null;
        if (this.obj.product_id != null) {
            temp_product_id = this.obj.product_id;
        }
        else {
            temp_product_id = this.obj.id;
        }
        if (this.user_id != null && this.user_id != 0) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            });
            this.http.post(this.base_url + 'api/favoriest_api/add_un_favorites', 'product_id=' + temp_product_id + '&user_id=' + this.user_id, { headers: headers }).subscribe(function (data) {
                console.log(data.json());
                _this.obj.favorites = data.json().favorites;
            }, function (error) {
            });
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
        }
    };
    PostDetailPage.prototype.view = function (item) {
        this.navCtrl.push(PostDetailPage_1, { item: item });
    };
    PostDetailPage.prototype.view_prf = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__profile_profile__["a" /* ProfilePage */], { user_id: id });
    };
    PostDetailPage.prototype.rate = function () {
        var _this = this;
        this.storage.ready().then(function () {
            _this.storage.get('user').then(function (obj) {
                if (obj != null) {
                    if (_this.obj.user_id == obj.id) {
                        var alert_1 = _this.alertCtrl.create({
                            message: 'You can not rate your own product',
                            buttons: ['Dismiss']
                        });
                        alert_1.present();
                        return;
                    }
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__modal_rating_modal_rating__["a" /* ModalRatingPage */], { 'product_id': _this.obj.id, 'product_user_id': _this.obj.user_id, 'user_id': obj.id });
                    modal.present();
                }
                else {
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
                    modal.present();
                    return;
                    //go to login 
                }
            });
        });
    };
    PostDetailPage.prototype.mail = function (item) {
        var _this = this;
        console.log(item);
        if (this.user_id == null) {
            //go to login page
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */]);
            modal.present();
            return;
        }
        var promtDialog = this.alertCtrl.create({
            title: 'Messages',
            inputs: [
                {
                    name: 'message',
                    placeholder: 'Message'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        ///send message via email system
                        _this.storage.ready().then(function () {
                            _this.storage.get('user').then(function (obj) {
                                if (obj.email == item.email) {
                                    var alert_2 = _this.alertCtrl.create({
                                        message: "you cannot send mail to yourself",
                                        buttons: ['Dismiss']
                                    });
                                    alert_2.present();
                                    return;
                                }
                                var post_data = 'email=' + item.email + '&message=' + data.message + '&user_name=' + obj.user_name + '&reply_to=' + obj.email;
                                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                });
                                _this.http.post(_this.base_url + 'api/users_api/send_enquiry', post_data, { headers: headers }).subscribe(function (data) {
                                    if (data.json().success == 0) {
                                        var toast = _this.toastCtrl.create({
                                            message: 'you just can send enquiry after 120 seconds',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                    else {
                                        var toast = _this.toastCtrl.create({
                                            message: 'Your messages have been sent, thank you !!!',
                                            duration: 3000
                                        });
                                        toast.present();
                                    }
                                }, function (error) {
                                });
                            });
                        });
                    }
                }
            ]
        });
        promtDialog.present();
    };
    return PostDetailPage;
}());
PostDetailPage = PostDetailPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
        selector: 'page-post-detail',template:/*ion-inline-start:"/Users/carlosdelcid/Apps/ionic_app/src/pages/post-detail/post-detail.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Post Detail</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div *ngIf="img_lists.length>0">\n    <ion-slides initialSlide="0">\n      <ion-slide *ngFor="let item of img_lists">\n        <img src="{{base_url + item.path}}" >\n      </ion-slide>\n    </ion-slides>\n  </div>\n  <ion-item>\n    <ion-avatar item-start (click)="view_prf(obj.user_id)">\n      <img *ngIf="obj.avt != null && obj.avt.indexOf(\'graph.facebook.com\')==-1" src="{{base_url + obj.avt}}">\n      <img *ngIf="obj.avt != null && obj.avt.indexOf(\'graph.facebook.com\')!=-1" src="{{obj.avt}}">\n      <img *ngIf="obj.avt == null" src="assets/avt.png">\n    </ion-avatar>\n    <div>\n      <h2 (click)="view_prf(obj.user_id)">{{obj.user_name}}</h2>\n      <p>posted at {{obj.created_at}}</p>\n    </div>\n    <div class="more">\n      <button ion-button icon-left clear [ngClass]="{\'active\' : (obj.report == true)}" (click)="report(obj.id)">\n        <ion-icon name="ios-flag-outline"></ion-icon>\n      </button>\n    </div>\n  </ion-item>\n  <div padding>\n    <h2 class="title">{{obj.title}}</h2>\n    <rating [(ngModel)]="obj.rating" readOnly="true" max="5" emptyStarIconName="star-outline" halfStarIconName="star-half" starIconName="star" nullable="false" (click)="rate()">\n    </rating>\n    <p class="price">{{\'price\' | translate}}: {{obj.price}}</p>\n    <p class="local">{{\'local\' | translate}}: {{obj.county_name}} - {{obj.cities_name}}</p>\n    <p class="purpose">{{\'purpose\' | translate}}:\n      <span *ngIf="obj.aim == 0">{{\'buy\' | translate}}</span>\n      <span *ngIf="obj.aim == 1">{{\'sale\' | translate}}</span>\n    </p>\n    <p class="condition">{{\'condition\' | translate}}:\n      <span *ngIf="obj.condition == 0">{{\'new\' | translate}}</span>\n      <span *ngIf="obj.condition == 1">{{\'second_hand\' | translate}}</span>\n    </p>\n    <p class="txt-content">{{obj.content}}</p>\n    <div class="action">\n      <button small claer color="light" round block ion-button (click)="call(obj.phone)">\n        <ion-icon name="ios-call"></ion-icon> {{\'call\' | translate }}\n      </button>\n      <button small claer color="light" round block ion-button [ngClass]="{\'active\' : (obj.favorites == true)}" (click)="favorites()">\n        <ion-icon name="ios-heart"></ion-icon> {{\'favorites\' | translate }}\n      </button>\n      <button small claer color="light" round block ion-button (click)="share(obj)">\n        <ion-icon name="md-share"></ion-icon> {{\'share\' | translate }}\n      </button>\n      <button small claer color="light" round block ion-button (click)="mail(obj)">\n        <ion-icon name="ios-mail"></ion-icon> {{\'mail\' | translate }}\n      </button>\n    </div>\n    <div class="attachment">\n      <ion-icon name="md-attach" item-start></ion-icon>\n      <span>{{\'attachments\' | translate }}</span>\n      <ion-row>\n        <ion-col col-3 *ngFor="let item of attachment; let i = index">\n          <img (click)="view(item)" src="{{base_url + item.image_path}}" alt="">\n        </ion-col>\n      </ion-row>\n      <button ion-button clear (click)="view_prf(obj.user_id)">{{\'view_more\' | translate }}</button>\n    </div>\n  </div>\n  <!--<div class="comment" padding>\n    <h4>Comments</h4>\n    <div class="wrapper-cmt"></div>\n  </div>-->\n</ion-content>\n'/*ion-inline-end:"/Users/carlosdelcid/Apps/ionic_app/src/pages/post-detail/post-detail.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_call_number__["a" /* CallNumber */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
], PostDetailPage);

var PostDetailPage_1;
//# sourceMappingURL=post-detail.js.map

/***/ })

},[233]);
//# sourceMappingURL=main.js.map